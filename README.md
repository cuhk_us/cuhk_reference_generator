# Overview

## What does this software do?
Receive local target from path planning and generate trajectory based on reflexxes and send result reference to UAV through mavlink protocal.

## Specifications

## Limitations
This node generates log files, please clean them if there is not enough disk space.

# ROS API

## nus\_reference\_generator

### Subscribed Topics
- */mavros/cmd/vehicle_cmd (mavros::Cmd)*
Commands from mavros(GCS).
- */mavros/position/local (geometry_msgs::PoseStamped)*
Local position data from PX4.
- */mavros/global_position/compass_hdg (std_msgs::Float64)*
Compass heading data from PX4.
- */mavros/global_position/global (sensor_msgs::NavSatFix)*
GPS data from PX4.
- */mavros/global_position/gps_vel (geometry_msgs::Vector3Stamped)*
GPS velocity data from PX4.
- */mavros/imu/data (sensor_msgs::Imu)*
IMU data from PX4.
- */mavros/imu/mag (sensor_msgs::MagneticField)*
IMU magnetic field data from PX4.
- *state/measurement (nus_msgs::StateWithCovarianceStamped)*
UAV current measured state from state estimator algorithm or simulator.
- *task_manager/current_target (geometry_msgs::PoseStamped)*
Current target from task manager.
- *vehicle_action (nus_reference_generator::action)*
For internal usage, should not be used from outside.
- *vehicle_action_response (nus_reference_generator::response)*
For internal usage, should not be used from outside.

### Published Topics
- *task_manager_start_cmd (std_msgs::Bool)*
Trigger for starting task manager.
- *task_manager_perching_point (nus_msgs::PerchingPoint)*
Waypoint to send to task manager.
- *mavlink/mavref_to_pixhawk (mavros::Mavlink)*
Should be obsoleted since furitdove d4. MAVLink reference message to pixhawk.
- *mavlink/ros_to_gcs (mavros::Mavlink)*
MAVLink messages to GCS.
- *region_of_interest (nus_msgs::RegionOfInterest)*
Set region of interest of image, this command is sent from GCS.
- *state/reference (nus_msgs::StateWithCovarianceStamped)*
UAV reference state, which should be sent to pixhawk or simulator.
- *state/reference/pose (geometry_msgs::PoseStamped)*
UAV reference pose, which is mainly for visulization purpose.
- *vehicle_action (nus_reference_generator::action)*
For internal usage, should not be used from outside.
- *vehicle_action_response (nus_reference_generator::response)*
For internal usage, should not be used from outside.

### Services
- *gps_to_local_position (nus_msgs::Gps2LocalPosition)*
Convert GPS to local position (NWU) based on initial GPS coordinate.
- *local_position_to_gps (nus_msgs::LocalPosition2Gps)*
Convert local position (NWU) to GPS based on initial GPS coordinate.

### Parameters
- *~self_system_id* (int, default: 1)
Self system id, used for MAVLink
- *~self_component_id* (int, default: 2)
Self component id, used for MAVLink
- *~enable_geofencing* (bool, default: false)
Switch for geofencing, haven't been tested.
- *~enable_hitl* (bool, default: false)
Switch for HITL simulation
- *pp_takeoff_maxVel_xy* (double, default: 0.5)
Takeoff max velocity of horizontal position, unit: m/s
- *pp_takeoff_maxVel_z* (double, default: 1.0)
Takeoff max velocity of height, unit: m/s
- *pp_takeoff_maxVel_c* (double default: 0.25)
Takeoff max velocity of heading, unit: radian/s
- *pp_takeoff_maxAcc_xy* (double ,default: 0.25)
Takeoff max acceleration of horizontal position, unit: m/s^2
- *pp_takeoff_maxAcc_z* (double, default: 0.5)
Takeoff max acceleration of height, unit: m/s^2
- *pp_takeoff_maxAcc_c* (double, default: 0.25)
Takeoff max acceleration of heading, unit: radian/s^2
- *pp_takeoff_maxJerk_xy* (double, default: 0.8)
Takeoff max jerk of horizontal position, unit: m/s^3
- *pp_takeoff_maxJerk_z* (double, default: 0.5)
Takeoff max jerk of height, unit: m/s^3
- *pp_takeoff_maxJerk_c* (double, default: 0.5)
Takeoff max jerk of heading, unit: m/s^3
- *pp_land_maxVel_xy* (double, default: 0.3)
Land max velocity of horizontal position, unit: m/s
- *pp_land_maxVel_z* (double, default: 0.3)
Land max velocity of height, unit: m/s
- *pp_land_maxVel_c* (double default: 0.3)
Land max velocity of heading, unit: radian/s
- *pp_land_maxAcc_xy* (double ,default: 0.15)
Land max acceleration of horizontal position, unit: m/s^2
- *pp_land_maxAcc_z* (double, default: 0.15)
Land max acceleration of height, unit: m/s^2
- *pp_land_maxAcc_c* (double, default: 0.15)
Land max acceleration of heading, unit: radian/s^2
- *pp_land_maxJerk_xy* (double, default: 0.8)
Land max jerk of horizontal position, unit: m/s^3
- *pp_land_maxJerk_z* (double, default: 0.5)
Land max jerk of height, unit: m/s^3
- *pp_land_maxJerk_c* (double, default: 0.5)
Land max jerk of heading, unit: m/s^3
- *pp_fly_maxVel_xy* (double, default: 1.0)
Fly max velocity of horizontal position, unit: m/s
- *pp_fly_maxVel_z* (double, default: 1.0)
Fly max velocity of height, unit: m/s
- *pp_fly_maxVel_c* (double default: 0.3)
Fly max velocity of heading, unit: radian/s
- *pp_fly_maxAcc_xy* (double ,default: 0.5)
Fly max acceleration of horizontal position, unit: m/s^2
- *pp_fly_maxAcc_z* (double, default: 0.5)
Fly max acceleration of height, unit: m/s^2
- *pp_fly_maxAcc_c* (double, default: 0.5)
Fly max acceleration of heading, unit: radian/s^2
- *pp_fly_maxJerk_xy* (double, default: 0.8)
Fly max jerk of horizontal position, unit: m/s^3
- *pp_fly_maxJerk_z* (double, default: 0.5)
Fly max jerk of height, unit: m/s^3
- *pp_fly_maxJerk_c* (double, default: 0.5)
Fly max jerk of heading, unit: m/s^3
