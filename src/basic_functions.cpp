// Copyright 2015-2017 Yuchao Hu
/*
 * basicFunctions.cpp
 *
 *  Created on: Jul 12, 2015
 *      Author: li
 */

#include "nus_reference_generator/basic_functions.h"

// ------------------------ parameters
namespace nus_reference_generator
{
double return_home_height = 5;

double takeoff_height = 1.0;  // TODO(ronky): positive or negative

// home position(on land) in global frame
double homeposition[3] = { 0, 0, 0 };

// ------------------------ parameters end

double t_start = 0;

double getElapsedTime()
{
  double t_elapsed = ros::Time::now().toSec() - t_start;
  return t_elapsed;
}

float DEG2RAD(float angle)
{
  return angle * M_PI / 180.0;
}

float RAD2DEG(float angle)
{
  return angle * 180.0 / M_PI;
}

}  // namespace nus_reference_generator
