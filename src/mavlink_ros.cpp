// Copyright 2015-2017 Yuchao Hu
/*
 *  mavlink_ros.cc
 *
 *  Created on: 2016
 *      Author: Yuchao Hu
 */

#include "nus_reference_generator/mavlink_ros.h"
#include <string>

namespace nus_reference_generator
{
MavlinkRos::MavlinkRos(ros::NodeHandle n)
{
  ros::NodeHandle private_nh_("~");
  int self_system_id;
  int self_component_id;
  private_nh_.param("self_system_id", self_system_id, 1);
  private_nh_.param("self_component_id", self_component_id, 2);
  self_system_id_ = self_system_id;
  self_component_id_ = self_component_id;

  ros_to_pixhawk_ref_pub_ = n.advertise<mavros::Mavlink>("/mavlink/mavref_to_pixhawk", 10);
  ros_to_gcs_pub_ = n.advertise<mavros::Mavlink>("/mavlink/ros_to_gcs", 10);
}

void MavlinkRos::send_ref(const mavlink_set_position_target_local_ned_t& ref_s) const
{
  mavlink_message_t mavmsg;
  mavros::Mavlink rosmsg;
  mavlink_msg_set_position_target_local_ned_encode(self_system_id_, self_component_id_, &mavmsg, &ref_s);
  copy_mavlink_to_ros(mavmsg, rosmsg);
  ros_to_pixhawk_ref_pub_.publish(rosmsg);
}

void MavlinkRos::copy_mavlink_to_ros(const mavlink_message_t& _mavmsg, mavros::Mavlink& _rosmsg) const
{
  _rosmsg.len = _mavmsg.len;
  _rosmsg.seq = _mavmsg.seq;
  _rosmsg.sysid = _mavmsg.sysid;
  _rosmsg.compid = _mavmsg.compid;
  _rosmsg.msgid = _mavmsg.msgid;

  _rosmsg.payload64.clear();
  // printf("[ros msg]sysid is %d , compid is %d", rmsg->sysid,rmsg->compid);
  //
  _rosmsg.payload64.reserve((_mavmsg.len + 7) / 8);
  for (size_t i = 0; i < (_mavmsg.len + 7) / 8; i++)
  {
    _rosmsg.payload64.push_back(_mavmsg.payload64[i]);
  }
}

void MavlinkRos::disp_on_gcs(const std::string& info, const int& severity) const
{
  mavlink_message_t mavmsg;
  mavros::Mavlink rosmsg;
  mavlink_statustext_t msg_to_gcs;
  for (size_t i = 0; i < info.length(); i += sizeof(msg_to_gcs.text))
  {
    strncpy(msg_to_gcs.text, info.substr(i, sizeof(msg_to_gcs.text)).c_str(), sizeof(msg_to_gcs.text));
    msg_to_gcs.severity = severity;
    mavlink_msg_statustext_encode(self_system_id_, self_component_id_, &mavmsg, &msg_to_gcs);
    copy_mavlink_to_ros(mavmsg, rosmsg);
    ros_to_gcs_pub_.publish(rosmsg);
  }
}

}  // namespace nus_reference_generator
