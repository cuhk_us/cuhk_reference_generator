// Copyright 2015-2017 Yuchao Hu
/*
 * log.cc
 *
 *  Created on: Jul 14, 2015
 *      Author: li
 *  Modified on: Sep 10, 2018
 *      Anthor: Mingjie 
 */

#include "nus_reference_generator/log.h"
#include <ros/package.h>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

namespace nus_reference_generator
{
Log::Log(ros::NodeHandle n,
         const std::shared_ptr<UavState>& _uav_state_ptr,
         const std::shared_ptr<ReflexxesGenerator>& _reflexxes_generator_ptr,
         const std::shared_ptr<Navigator>& _navigator_ptr)
  : uav_state_ptr_(_uav_state_ptr), reflexxes_generator_ptr_(_reflexxes_generator_ptr), navigator_ptr_(_navigator_ptr)
{
  keep_running_ = false;
  main_loop_timer_ = n.createTimer(ros::Duration(0.02), &Log::Loop, this);

  time_t t = time(0);  // get time now
  struct tm now;
  localtime_r(&t, &now);

  std::string dir = ros::package::getPath("nus_reference_generator");
  std::string path = dir + "/log/";
  std::string filename_data = std::to_string(now.tm_year + 1900) + std::string("_") + std::to_string(now.tm_mon + 1) +
                              std::string("_") + std::to_string(now.tm_mday) + std::string("_") +
                              std::to_string(now.tm_hour) + std::string("_") + std::to_string(now.tm_min) +
                              std::string("_") + std::to_string(now.tm_sec) + std::string("_data.txt");

  logDataFile.open((path + filename_data).c_str(), std::ios::out | std::ios::trunc);
  std::cout << "log file saved at" << (path + filename_data).c_str() << std::endl;

  std::string filename_cmm = std::to_string(now.tm_year + 1900) + std::string("_") + std::to_string(now.tm_mon + 1) +
                             std::string("_") + std::to_string(now.tm_mday) + std::string("_") +
                             std::to_string(now.tm_hour) + std::string("_") + std::to_string(now.tm_min) +
                             std::string("_") + std::to_string(now.tm_sec) + std::string("_cmm.txt");
  std::ofstream logCMMFile;
  logCMMFile.open((path + filename_cmm).c_str(), std::ios::out | std::ios::trunc);

}

Log::~Log()
{
  Stop();
}

void Log::Start()
{
  if (!keep_running_)
  {
    keep_running_ = true;
  }
}

void Log::Stop()
{
  if (keep_running_)
  {
    keep_running_ = false;
  }
}

void Log::Loop(const ros::TimerEvent& event)
{

  if(keep_running_ && ros::ok())
  {
    LogData(logDataFile);
  }
}

void Log::LogData(std::ofstream& file)
{
  UAV_STATE mea = uav_state_ptr_->getUAVState();

  std::vector<double> ref_pos = reflexxes_generator_ptr_->getRefPos();
  std::vector<double> ref_vel = reflexxes_generator_ptr_->getRefVel();
  std::vector<double> ref_acc = reflexxes_generator_ptr_->getRefAcc();

  file << nus_reference_generator::getElapsedTime() << "  ";
  file << ref_pos.at(0) << "  " << ref_pos.at(1) << "  " << ref_pos.at(2) << "  " << ref_pos.at(3) << "  ";
  file << ref_vel.at(0) << "  " << ref_vel.at(1) << "  " << ref_vel.at(2) << "  " << ref_vel.at(3) << "  ";
  file << ref_acc.at(0) << "  " << ref_acc.at(1) << "  " << ref_acc.at(2) << "  " << ref_acc.at(3) << "  ";
  file << mea.x_pos << "  " << mea.y_pos << "  " << mea.z_pos << "  " << mea.c_pos << "  ";
  file << mea.x_vel << "  " << mea.y_vel << "  " << mea.z_vel << "  " << mea.c_vel << "  ";
  file << mea.x_acc << "  " << mea.y_acc << "  " << mea.z_acc << "  " << mea.c_acc << "  ";
  file << mea.roll << "  " << mea.pitch << "  " << mea.yaw << "  ";
  file << navigator_ptr_->get_cur_behavior() << " ";
  file << std::endl;
}

}  // namespace nus_reference_generator
