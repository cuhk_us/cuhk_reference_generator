// Copyright 2015-2017 Yuchao Hu
/*
 * navigator.cc
 *
 *  Created on: Jul 14, 2015
 *      Author: li
 */

#include "nus_reference_generator/navigator.h"
#include <sensor_msgs/Imu.h>
#include <iostream>
#include <string>
#include "nus_reference_generator/mavlink_ros.h"
#include "nus_reference_generator/uav_state.h"

namespace nus_reference_generator
{
Navigator::Navigator(ros::NodeHandle n, const std::shared_ptr<MavlinkRos>& _mavlink_ros_ptr,
                     const std::shared_ptr<UavState>& _uav_state_ptr,
                     const std::shared_ptr<ReflexxesGenerator>& _reflexxes_generator_ptr)
  : mavlink_ros_ptr_(_mavlink_ros_ptr)
  , uav_state_ptr_(_uav_state_ptr)
  , reflexxes_generator_ptr_(_reflexxes_generator_ptr)
  , pitch_mag(0)
  , roll_mag(0)
  , NavigateWithTraj(false)
{
  memset(&ref_s, 0, sizeof(ref_s));

  action_sub =
      n.subscribe<nus_reference_generator::action>("vehicle_action", 10, &Navigator::vehicle_action_callback, this);
  response_pub = n.advertise<nus_reference_generator::response>("vehicle_action_response", 10);
  state_reference_pub_ = n.advertise<nus_msgs::StateWithCovarianceMaskStamped>("/state/reference", 1);
  state_reference_pose_pub_ = n.advertise<geometry_msgs::PoseStamped>("/state/reference/pose", 1);

  n.param("NavigateWithTraj", NavigateWithTraj, false);

  int self_system_id;
  n.param("self_system_id", self_system_id, 1);

  std::string str_self_system_id =
      "/sys_id_" + static_cast<std::ostringstream*>(&(std::ostringstream() << self_system_id))->str();
  servo_value_sub =
      n.subscribe<sensor_msgs::Imu>("/mavros/imu/data" + str_self_system_id, 10, &Navigator::servoValueCallback, this);

   main_loop_timer_ = n.createTimer(ros::Duration(0.02), &Navigator::Loop, this);

  cur_action.behavior = IDLE;
  cur_action.frameid = 0;  //  0 for global frame
  cur_action.guid_signal_source = 0;
  cur_action.params.clear();
  count = 0;
  is_land_reached_ = false;

  for (int i = 0; i < MAX_ACTION_NUMBER; i++)
  {
    init_flags[i] = false;
  }
  report_finished_flag = false;
  behavior_names.push_back("IDLE");
  behavior_names.push_back("TAKEOFF");
  behavior_names.push_back("HFLY");
  behavior_names.push_back("NAVIGATE_TO");
  behavior_names.push_back("VISION_GUIDE_DESCEND");
  behavior_names.push_back("DESCEND_GRASP_ASCEND");
  behavior_names.push_back("DESCEND_UNLOAD_ASCEND");
  behavior_names.push_back("RETURN_HOME");
  behavior_names.push_back("LAND");
  behavior_names.push_back("OPEN_CLAW");
  behavior_names.push_back("CLOSE_CLAW");
  behavior_names.push_back("11");
  behavior_names.push_back("12");
  behavior_names.push_back("13");
  behavior_names.push_back("14");
  behavior_names.push_back("15");
  behavior_names.push_back("16");
  behavior_names.push_back("17");
  behavior_names.push_back("18");
  behavior_names.push_back("19");
  behavior_names.push_back("20");

  keep_running_ = false;
}

Navigator::~Navigator()
{
  Stop();
}

void Navigator::Start()
{
  if (!keep_running_)
  {
    keep_running_ = true;
  }
}

void Navigator::Stop()
{
  if (keep_running_)
  {
    keep_running_ = false;
   // thread_point_->join();
  }
}

void Navigator::vehicle_action_callback(const nus_reference_generator::action::ConstPtr& msg)
{
  try
  {
    if (msg->behavior == last_action_msg_.behavior && msg->params == last_action_msg_.params &&
        msg->guid_signal_source == last_action_msg_.guid_signal_source && msg->frameid == last_action_msg_.frameid)
    {
      return;
    }
    cur_action = *msg.get();
    last_action_msg_ = cur_action;
    cur_response.cur_behavior = cur_action.behavior;
    cur_response.behavior_finished = false;
    cur_response.behavior_received = true;
    response_pub.publish(cur_response);
    std::string message_to_gcs;
    message_to_gcs.append("[nus_reference_generator::Navigator] Behavior received: ");
    message_to_gcs.append(behavior_names[cur_action.behavior]);
    mavlink_ros_ptr_->disp_on_gcs(message_to_gcs.c_str(), MAV_SEVERITY_INFO);
    init_flags[cur_action.behavior] = false;
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("[nus_reference_generator::Navigator::vehicle_action_callback]");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

void Navigator::clear_all_flags_except(int current_behavior)
{
  for (int i = 0; i < MAX_ACTION_NUMBER; i++)
  {
    if (i != current_behavior)
    {
      init_flags[i] = false;
    }
  }
  report_finished_flag = false;
}

void Navigator::report_task_finished(int behavior)
{
  cur_response.cur_behavior = behavior;
  cur_response.behavior_finished = true;
  response_pub.publish(cur_response);
}

void Navigator::handle_takeoff()
{
  //ROS_INFO("c_pos: %f, c_vel: %f, state_yaw: %f", uav_state.c_pos, uav_state.c_vel, uav_state.yaw);
  if (!init_flags[TAKEOFF])
  {
    

    home_x = uav_state.x_pos;
    home_y = uav_state.y_pos;
    home_z = uav_state.z_pos;
    home_yaw = uav_state.yaw ;
    home_latitude_ = uav_gps_coordinate_.latitude;
    home_longitude_ = uav_gps_coordinate_.longitude;
    home_altitude_ = uav_gps_coordinate_.altitude;

    target.x = uav_state.x_pos;
    target.y = uav_state.y_pos;
    target.c = home_yaw;//uav_state.yaw ; //uav_state.c_pos;
    target.z = cur_action.params.at(0);

    init_flags[TAKEOFF] = true;
    reflexxes_generator_ptr_->Reset(ReflexxesGenerator::kTakeOff);
    reflexxes_generator_ptr_->Initialize(uav_state);
    clear_all_flags_except(TAKEOFF);
  }

  ref_s = reflexxes_generator_ptr_->planningSimple(target);
  // ref_s.yaw = target.c;
  ref_s.type_mask = 0b0000000000000000;
  if (reflexxes_generator_ptr_->GetReflexxesStatus() == 1 && !report_finished_flag)
  {  // target reached
    report_finished_flag = true;
    report_task_finished(TAKEOFF);
    mavlink_ros_ptr_->disp_on_gcs("[nus_reference_generator::Navigator] TAKEOFF finished!", MAV_SEVERITY_INFO);
  }
}

void Navigator::handle_hfly()
{
  if (!init_flags[HFLY])
  {
    double temp_x, temp_y;
    temp_x = cos(uav_state.c_pos) * cur_action.params.at(0) - sin(uav_state.c_pos) * cur_action.params.at(1);
    temp_y = sin(uav_state.c_pos) * cur_action.params.at(0) + cos(uav_state.c_pos) * cur_action.params.at(1);

    target.x = uav_state.x_pos + temp_x;
    target.y = uav_state.y_pos + temp_y;
    target.z = uav_state.z_pos + cur_action.params.at(2);

    target.c = home_yaw; //DEG2RAD(cur_action.params.at(3)) + uav_state.c_pos;
    INPI(target.c);
    reflexxes_generator_ptr_->Reset(ReflexxesGenerator::kGps);
    reflexxes_generator_ptr_->Initialize(uav_state);
    init_flags[HFLY] = true;
    clear_all_flags_except(HFLY);
  }

  ref_s = reflexxes_generator_ptr_->planningSimple(target);
  ref_s.type_mask = 0b0000000000000000;
  if (reflexxes_generator_ptr_->GetReflexxesStatus() == 1 && !report_finished_flag)
  {
    report_task_finished(HFLY);
    report_finished_flag = true;
    mavlink_ros_ptr_->disp_on_gcs("[nus_reference_generator::Navigator] HFLY finished!", MAV_SEVERITY_INFO);
  }
}

void Navigator::handle_navigate_to()
{
  if (!init_flags[NAVIGATE_TO])
  {
    if (0 != cur_action.frameid)
    {
      cur_action.frameid = 0;
      ROS_WARN("[nus_reference_generator::Navigator::handle_navigate_to] unsupported frameid %d, force set to 0",
               cur_action.frameid);
    }
    // if navigate with reflexxes, assign value to target and reset reflexxes params
    if(!NavigateWithTraj){

    target.x = cur_action.params[0];
    target.y = cur_action.params[1];
    target.z = cur_action.params[2];
    target.c = home_yaw;//cur_action.params[3];

    // store the target velocity
    target.vx = cur_action.params[4];
    target.vy = cur_action.params[5];
    target.vz = cur_action.params[6];
    // reflexxes_generator_ptr_->initializeGPSPathPlanning(uav_state); //TODO:
    // Jin Why initialize again with state?
    reflexxes_generator_ptr_->ResetLimit(ReflexxesGenerator::kGps);
    }

    init_flags[NAVIGATE_TO] = true;
    clear_all_flags_except(NAVIGATE_TO);
  }
  // // if navigate with reflexxes, get reference from reflexxes and check whether target has reached
  if(!NavigateWithTraj){
    ref_s = reflexxes_generator_ptr_->planningSimple(target);
    ref_s.type_mask = 0b0000000000000000;

    if (reflexxes_generator_ptr_->GetReflexxesStatus() == 1 && !report_finished_flag)
    {
      report_finished_flag = true;
      report_task_finished(NAVIGATE_TO);
      mavlink_ros_ptr_->disp_on_gcs("[nus_reference_generator::Navigator] Navigate to behavior finished!",
                                  MAV_SEVERITY_INFO);
    }
  }
  else{
    // If navaigation with trajectory, assign targets to ref_s and no need for reach check
    ref_s.x = cur_action.params[0];
    ref_s.y = cur_action.params[1];
    ref_s.z = cur_action.params[2];

    ref_s.yaw = cur_action.params[3];

    ref_s.vx = cur_action.params[4];
    ref_s.vy = cur_action.params[5];
    ref_s.vz = cur_action.params[6]; 
   
    ref_s.afx = cur_action.params[7];
    ref_s.afy = cur_action.params[8];
    ref_s.afz = cur_action.params[9];

    ref_s.yaw_rate = 0;
    ref_s.type_mask = 0b0000000000000000;
  }
  
}

void Navigator::handle_return_home()
{
  if (!init_flags[RETURN_HOME])
  {
    target.x = home_x;
    target.y = home_y;
    target.c = home_yaw;//uav_state.c_pos;
    target.z = -return_home_height + home_z;
    init_flags[RETURN_HOME] = true;
    reflexxes_generator_ptr_->Initialize(uav_state);
    reflexxes_generator_ptr_->Reset(ReflexxesGenerator::kGps);  // FIXME: init then reset, are u sure?
    clear_all_flags_except(RETURN_HOME);
  }

  ref_s = reflexxes_generator_ptr_->planningSimple(target);
  ref_s.type_mask = 0b0000000000000000;
  if (reflexxes_generator_ptr_->GetReflexxesStatus() == 1 && report_finished_flag == false)
  {
    // target reached
    report_finished_flag = true;
    report_task_finished(RETURN_HOME);
    mavlink_ros_ptr_->disp_on_gcs("[nus_reference_generator::Navigator] Return home finished!", MAV_SEVERITY_INFO);
  }
}
void Navigator::handle_land()
{  
  // ROS_INFO("[navigator] handling land: %f %f %f ", ref_s.x, ref_s.y, ref_s.yaw);
  if (!init_flags[LAND])
  {
    // use the current reference as target, do not initialize using current
    // measurement
    target.x = ref_s.x;
    target.y = ref_s.y;
    target.c = home_yaw; //uav_state.yaw;// ref_s.yaw;
    target.z = 100;  // FIXME: here the target z is relative height (NED)

    init_flags[LAND] = true;
    reflexxes_generator_ptr_->Initialize(uav_state);
    reflexxes_generator_ptr_->Reset(ReflexxesGenerator::kLand);
    clear_all_flags_except(LAND);
    is_land_reached_ = false;
  }

  ref_s = reflexxes_generator_ptr_->planningSimple(target);
  // ref_s.yaw = target.c;
  ref_s.type_mask = 0b0000000000000000;

  if (ref_s.z > 99 && -uav_state.z_pos < 0.1)
  {
    // FIXME: better method to detect target reached?

    if (false == is_land_reached_)
    {
      land_reached_time_ = ros::Time::now();
      is_land_reached_ = true;
    }
    else if ((ros::Time::now() - land_reached_time_).toSec() > 5)
    {
      report_task_finished(LAND);
      cur_action.behavior = IDLE;
      mavlink_ros_ptr_->disp_on_gcs("[nus_reference_generator::Navigator] LAND finished!", MAV_SEVERITY_INFO);
      ROS_INFO("[nus_reference_generator::Navigator] Landing Finished");
      is_land_reached_ = false;
    }
  }
}

void Navigator::Loop(const ros::TimerEvent& event)
{
  // ROS_INFO("[nus_reference_generator::Navigator] Init ready.");

  if(keep_running_ && ros::ok())
  {
    // loop_count++;
    // uav_state_ptr_->print_state();
    uav_state = uav_state_ptr_->getUAVState();
    uav_gps_coordinate_ = uav_state_ptr_->getGPSCoordinate();

    switch (cur_action.behavior)
    {
      case IDLE:
      {
        ref_s.x = uav_state.x_pos;
        ref_s.y = uav_state.y_pos;
        ref_s.z = uav_state.z_pos;
        ref_s.yaw = uav_state.c_pos;
        ref_s.vx = uav_state.x_vel;
        ref_s.vy = uav_state.y_vel;
        ref_s.vz = uav_state.z_vel;
        ref_s.afx = 0;
        ref_s.afy = 0;
        ref_s.afz = 0;
        ref_s.yaw_rate = 0;
        ref_s.type_mask = 0b0000111111111111;
        reflexxes_generator_ptr_->Initialize(uav_state);
        if (!init_flags[IDLE])
        {
          init_flags[IDLE] = true;
        }
        break;
      }
      case TAKEOFF:
      {
        handle_takeoff();
        break;
      }
      case HFLY:
      {
        handle_hfly();
        break;
      }
      case NAVIGATE_TO:
      {
        handle_navigate_to();
        break;
      }
      case RETURN_HOME:
      {
        handle_return_home();
        break;
      }
      case LAND:
      {
        handle_land();
        break;
      }
      default:
      {
        break;
      }
    }

    
    // ROS_INFO("[navigator] %f %f %f %f %f %f", ref_s.x, ref_s.y, ref_s.z, ref_s.vx, ref_s.vy, ref_s.vz);
    /* publish reference state */
    /* NED -> forward/left/up */
    nus_msgs::StateWithCovarianceMaskStamped msg;
    msg.header.stamp = ros::Time::now();
    msg.header.frame_id = "/map";
    msg.pose.pose.position.x = ref_s.x;
    msg.pose.pose.position.y = -ref_s.y;
    msg.pose.pose.position.z = -ref_s.z;
    tf::Quaternion ref_quaternion;
    ref_quaternion.setRPY(0, 0, -ref_s.yaw);
    tf::quaternionTFToMsg(ref_quaternion, msg.pose.pose.orientation);
    msg.velocity.twist.linear.x = ref_s.vx;
    msg.velocity.twist.linear.y = -ref_s.vy;
    msg.velocity.twist.linear.z = -ref_s.vz;
    msg.acceleration.accel.linear.x = ref_s.afx;
    msg.acceleration.accel.linear.y = -ref_s.afy;
    msg.acceleration.accel.linear.z = -ref_s.afz;
    msg.type_mask = ref_s.type_mask;
    state_reference_pub_.publish(msg);


    if (state_reference_pose_pub_.getNumSubscribers() > 0)
    {
      geometry_msgs::PoseStamped ref_msg;
      ref_msg.header = msg.header;
      ref_msg.pose = msg.pose.pose;
      state_reference_pose_pub_.publish(ref_msg);
    }

  }
}

void Navigator::servoValueCallback(const sensor_msgs::Imu::ConstPtr& imuPtr)
{
  try
  {
    tf::Quaternion q;
    tf::quaternionMsgToTF(imuPtr->orientation, q);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    pitch_mag = -pitch / (45.0 / 180 * 3.1415) - 0.1;
    roll_mag = -roll / (45.0 / 180 * 3.1415);

    if (pitch_mag > 0.23)
      pitch_mag = 0.23;
    else if (pitch_mag < -0.47)
      pitch_mag = -0.47;

    if (roll_mag > 0.3)
      roll_mag = 0.3;
    else if (roll_mag < -0.25)
      roll_mag = -0.25;
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("[nus_reference_generator::Navigator::servoValueCallback]");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

}  // namespace nus_reference_generator
