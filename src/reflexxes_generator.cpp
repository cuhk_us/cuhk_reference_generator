// Copyright 2015-2017 Yuchao Hu
/*
 * reflexxes_generator.cc
 *
 *  Created on: Jul 14, 2015
 *      Author: li
 */

#include "nus_reference_generator/reflexxes_generator.h"
#include <boost/thread/lock_guard.hpp>
#include <vector>

namespace nus_reference_generator
{
ReflexxesGenerator::ReflexxesGenerator() : velRML(NULL), velOP(NULL), velIP(NULL)
{
  memset(&temp_ref, 0, sizeof(temp_ref));

  ros::NodeHandle private_nh_("~");

  private_nh_.param("pp_takeoff_maxVel_xy", pp_takeoff_maxVel_xy, 0.5);
  private_nh_.param("pp_takeoff_maxVel_z", pp_takeoff_maxVel_z, 1.0);
  private_nh_.param("pp_takeoff_maxVel_c", pp_takeoff_maxVel_c, 0.25);
  private_nh_.param("pp_takeoff_maxAcc_xy", pp_takeoff_maxAcc_xy, 0.25);
  private_nh_.param("pp_takeoff_maxAcc_z", pp_takeoff_maxAcc_z, 0.5);
  private_nh_.param("pp_takeoff_maxAcc_c", pp_takeoff_maxAcc_c, 0.25);
  private_nh_.param("pp_takeoff_maxJerk_xy", pp_takeoff_maxJerk_xy, 0.8);
  private_nh_.param("pp_takeoff_maxJerk_z", pp_takeoff_maxJerk_z, 0.5);
  private_nh_.param("pp_takeoff_maxJerk_c", pp_takeoff_maxJerk_c, 0.5);

  private_nh_.param("pp_land_maxVel_xy", pp_land_maxVel_xy, 0.5);
  private_nh_.param("pp_land_maxVel_z", pp_land_maxVel_z, 0.5);
  private_nh_.param("pp_land_maxVel_c", pp_land_maxVel_c, 0.2);
  private_nh_.param("pp_land_maxAcc_xy", pp_land_maxAcc_xy, 0.5);
  private_nh_.param("pp_land_maxAcc_z", pp_land_maxAcc_z, 0.5);
  private_nh_.param("pp_land_maxAcc_c", pp_land_maxAcc_c, 0.2);
  private_nh_.param("pp_land_maxJerk_xy", pp_land_maxJerk_xy, 0.8);
  private_nh_.param("pp_land_maxJerk_z", pp_land_maxJerk_z, 0.5);
  private_nh_.param("pp_land_maxJerk_c", pp_land_maxJerk_c, 0.2);

  private_nh_.param("pp_fly_maxVel_xy", pp_fly_maxVel_xy, 0.5);
  private_nh_.param("pp_fly_maxVel_z", pp_fly_maxVel_z, 0.5);
  private_nh_.param("pp_fly_maxVel_c", pp_fly_maxVel_c, 0.2);
  private_nh_.param("pp_fly_maxAcc_xy", pp_fly_maxAcc_xy, 0.5);
  private_nh_.param("pp_fly_maxAcc_z", pp_fly_maxAcc_z, 0.5);
  private_nh_.param("pp_fly_maxAcc_c", pp_fly_maxAcc_c, 0.2);
  private_nh_.param("pp_fly_maxJerk_xy", pp_fly_maxJerk_xy, 0.8);
  private_nh_.param("pp_fly_maxJerk_z", pp_fly_maxJerk_z, 0.5);
  private_nh_.param("pp_fly_maxJerk_c", pp_fly_maxJerk_c, 0.2);

  // TODO(ronky): keep same with pathplanning
  Flags.SynchronizationBehavior = RMLPositionFlags::ONLY_TIME_SYNCHRONIZATION;

  const size_t reflexxes_position_number_dof = 3;
  RML = new ReflexxesAPI(reflexxes_position_number_dof, CYCLE_TIME);
  IP = new RMLPositionInputParameters(reflexxes_position_number_dof);
  OP = new RMLPositionOutputParameters(reflexxes_position_number_dof);

  const size_t reflexxes_heading_number_dof = 1;
  reflexxes_heading_motion_ptr_ = std::make_shared<ReflexxesAPI>(reflexxes_heading_number_dof, CYCLE_TIME);
  reflexxes_heading_input_ptr_ = std::make_shared<RMLPositionInputParameters>(reflexxes_heading_number_dof);
  reflexxes_heading_output_ptr_ = std::make_shared<RMLPositionOutputParameters>(reflexxes_heading_number_dof);
  reflexxes_heading_flags_.SynchronizationBehavior = RMLPositionFlags::NO_SYNCHRONIZATION;

  Reset(kTakeOff);
}

ReflexxesGenerator::~ReflexxesGenerator()
{
  if (RML)
  {
    delete RML;
    RML = NULL;
  }
  if (IP)
  {
    delete IP;
    IP = NULL;
  }
  if (OP)
  {
    delete OP;
    OP = NULL;
  }

  if (velRML)
  {
    delete velRML;
    velRML = NULL;
  }
  if (velIP)
  {
    delete velIP;
    velIP = NULL;
  }
  if (velOP)
  {
    delete velOP;
    velOP = NULL;
  }
}

void ReflexxesGenerator::Initialize(const UAV_STATE& state)
{
  boost::lock_guard<boost::mutex> lock(reflexxes_current_state_mutex_);

  // ********************************************************************
  // initialize the RML for position trajectory generation

  IP->CurrentPositionVector->VecData[0] = state.x_pos;
  IP->CurrentPositionVector->VecData[1] = state.y_pos;
  IP->CurrentPositionVector->VecData[2] = state.z_pos;

  IP->CurrentVelocityVector->VecData[0] = state.x_vel;
  IP->CurrentVelocityVector->VecData[1] = state.y_vel;
  IP->CurrentVelocityVector->VecData[2] = state.z_vel;

  IP->CurrentAccelerationVector->VecData[0] = 0;
  IP->CurrentAccelerationVector->VecData[1] = 0;
  IP->CurrentAccelerationVector->VecData[2] = 0;

  // ********************************************************************
  // initialize the RML for heading trajectory generation

  reflexxes_heading_input_ptr_->CurrentPositionVector->VecData[0] = state.yaw;//state.c_pos;
  reflexxes_heading_input_ptr_->CurrentVelocityVector->VecData[0] = state.c_vel;
  reflexxes_heading_input_ptr_->CurrentAccelerationVector->VecData[0] = 0;

  uninpi_c = state.yaw ;//c_pos;

  // ********************************************************************

  is_initialized_ = true;
}

mavlink_set_position_target_local_ned_t ReflexxesGenerator::planningSimple(const REFERENCE& target)
{
  boost::lock_guard<boost::mutex> lock(reflexxes_current_state_mutex_);

  // ********************************************************************
  // set up the target position
  
  // ROS_INFO("[Relexxes] %f %f %f %f %f %f", target.x, target.y, target.z, IP->CurrentPositionVector->VecData[0],IP->CurrentPositionVector->VecData[1], IP->CurrentPositionVector->VecData[2]);
  IP->TargetPositionVector->VecData[0] = target.x;
  IP->TargetPositionVector->VecData[1] = target.y;
  IP->TargetPositionVector->VecData[2] = target.z;

  IP->TargetVelocityVector->VecData[0] = target.vx;
  IP->TargetVelocityVector->VecData[1] = target.vy;
  IP->TargetVelocityVector->VecData[2] = target.vz;

  // ********************************************************************
  // Calling the Reflexxes OTG algorithm

  int status = RML->RMLPosition(*IP, OP, Flags);
  // ********************************************************************
  // set up the target heading

  float psi_diff = target.c - uninpi_c;
  INPI(psi_diff);
  uninpi_c += psi_diff;

  reflexxes_heading_input_ptr_->TargetPositionVector->VecData[0] = uninpi_c;
  reflexxes_heading_input_ptr_->TargetVelocityVector->VecData[0] = 0;

  // ********************************************************************
  // Calling the Reflexxes OTG algorithm

  int status_heading = reflexxes_heading_motion_ptr_->RMLPosition(
      *reflexxes_heading_input_ptr_, reflexxes_heading_output_ptr_.get(), reflexxes_heading_flags_);

  // ********************************************************************
  // check the status

  if (status < 0 || status_heading < 0)
  {
    std::cout << " !!! Error in simple path planning." << std::endl;
  }
  else
  {
    SetGeneratedRef(temp_ref);
  }

  // ********************************************************************
  // return the status

  reflexxes_status_ = status;


  // ensure that reference to UAV is in pi

  // ********************************************************************
  // update the current states

  *IP->CurrentPositionVector = *OP->NewPositionVector;
  *IP->CurrentVelocityVector = *OP->NewVelocityVector;
  *IP->CurrentAccelerationVector = *OP->NewAccelerationVector;

  *reflexxes_heading_input_ptr_->CurrentPositionVector = *reflexxes_heading_output_ptr_->NewPositionVector;
  *reflexxes_heading_input_ptr_->CurrentVelocityVector = *reflexxes_heading_output_ptr_->NewVelocityVector;
  *reflexxes_heading_input_ptr_->CurrentAccelerationVector = *reflexxes_heading_output_ptr_->NewAccelerationVector;
  // ********************************************************************

  return temp_ref;
}

mavlink_set_position_target_local_ned_t ReflexxesGenerator::planningWithState(const REFERENCE& target,
                                                                              const UAV_STATE& state)
{
  boost::lock_guard<boost::mutex> lock(reflexxes_current_state_mutex_);

  // ********************************************************************
  // set up current conditions

  IP->CurrentPositionVector->VecData[0] = state.x_pos;
  IP->CurrentPositionVector->VecData[1] = state.y_pos;
  IP->CurrentPositionVector->VecData[2] = state.z_pos;

  IP->CurrentVelocityVector->VecData[0] = state.x_vel;
  IP->CurrentVelocityVector->VecData[1] = state.y_vel;
  IP->CurrentVelocityVector->VecData[2] = state.z_vel;

  IP->CurrentAccelerationVector->VecData[0] = state.x_acc;
  IP->CurrentAccelerationVector->VecData[1] = state.y_acc;
  IP->CurrentAccelerationVector->VecData[2] = state.z_acc;

  IP->CurrentPositionVector->VecData[3] = state.c_pos;
  IP->CurrentVelocityVector->VecData[3] = state.c_vel;
  IP->CurrentAccelerationVector->VecData[3] = state.c_acc;

  // ********************************************************************
  // set up the target position

  IP->TargetPositionVector->VecData[0] = target.x;
  IP->TargetPositionVector->VecData[1] = target.y;
  IP->TargetPositionVector->VecData[2] = target.z;

  IP->TargetVelocityVector->VecData[0] = 0;
  IP->TargetVelocityVector->VecData[1] = 0;
  IP->TargetVelocityVector->VecData[2] = 0;

  // ********************************************************************
  // Calling the Reflexxes OTG algorithm

  int status = RML->RMLPosition(*IP, OP, Flags);

  // ********************************************************************
  // set up the target heading

  float psi_diff = target.c - uninpi_c;
  INPI(psi_diff);
  uninpi_c += psi_diff;

  reflexxes_heading_input_ptr_->TargetPositionVector->VecData[0] = uninpi_c;
  reflexxes_heading_input_ptr_->TargetVelocityVector->VecData[0] = 0;

  // ********************************************************************
  // Calling the Reflexxes OTG algorithm

  int status_heading = reflexxes_heading_motion_ptr_->RMLPosition(
      *reflexxes_heading_input_ptr_, reflexxes_heading_output_ptr_.get(), reflexxes_heading_flags_);

  // ********************************************************************
  // check the status

  if (status < 0 || status_heading < 0)
  {
    std::cout << " !!! Error in simple path planning." << std::endl;
  }
  else
  {
    SetGeneratedRef(temp_ref);
  }

  // ********************************************************************
  // return the status

  reflexxes_status_ = status;

  // ensure that reference to UAV is in pi

  // ********************************************************************
  // update the current states

  *IP->CurrentPositionVector = *OP->NewPositionVector;
  *IP->CurrentVelocityVector = *OP->NewVelocityVector;
  *IP->CurrentAccelerationVector = *OP->NewAccelerationVector;

  *reflexxes_heading_input_ptr_->CurrentPositionVector = *reflexxes_heading_output_ptr_->NewPositionVector;
  *reflexxes_heading_input_ptr_->CurrentVelocityVector = *reflexxes_heading_output_ptr_->NewVelocityVector;
  *reflexxes_heading_input_ptr_->CurrentAccelerationVector = *reflexxes_heading_output_ptr_->NewAccelerationVector;
  // ********************************************************************

  return temp_ref;
}

void ReflexxesGenerator::SetGeneratedRef(mavlink_set_position_target_local_ned_t& temp_ref)
{
  boost::lock_guard<boost::mutex> lock(pp_ref_mutex_);

  temp_ref.x = OP->NewPositionVector->VecData[0];
  temp_ref.y = OP->NewPositionVector->VecData[1];
  temp_ref.z = OP->NewPositionVector->VecData[2];
  temp_ref.yaw = reflexxes_heading_output_ptr_->NewPositionVector->VecData[0];

  temp_ref.vx = OP->NewVelocityVector->VecData[0];
  temp_ref.vy = OP->NewVelocityVector->VecData[1];
  temp_ref.vz = OP->NewVelocityVector->VecData[2];
  temp_ref.yaw_rate = reflexxes_heading_output_ptr_->NewVelocityVector->VecData[0];

  temp_ref.afx = OP->NewAccelerationVector->VecData[0];
  temp_ref.afy = OP->NewAccelerationVector->VecData[1];
  temp_ref.afz = OP->NewAccelerationVector->VecData[2];

  INPI(temp_ref.yaw);
}

// \f get the reference from the current reflexxes
void ReflexxesGenerator::GetCurrentRef(mavlink_set_position_target_local_ned_t& current_ref) const
{
  current_ref.x = OP->NewPositionVector->VecData[0];
  current_ref.y = OP->NewPositionVector->VecData[1];
  current_ref.z = OP->NewPositionVector->VecData[2];
  current_ref.yaw = reflexxes_heading_output_ptr_->NewPositionVector->VecData[0];

  current_ref.vx = OP->NewVelocityVector->VecData[0];
  current_ref.vy = OP->NewVelocityVector->VecData[1];
  current_ref.vz = OP->NewVelocityVector->VecData[2];
  current_ref.yaw_rate = reflexxes_heading_output_ptr_->NewVelocityVector->VecData[0];

  current_ref.afx = OP->NewAccelerationVector->VecData[0];
  current_ref.afy = OP->NewAccelerationVector->VecData[1];
  current_ref.afz = OP->NewAccelerationVector->VecData[2];

  INPI(current_ref.yaw);
}

std::vector<double> ReflexxesGenerator::getRefPos()
{
  boost::lock_guard<boost::mutex> lock(pp_ref_mutex_);
  std::vector<double> ref_pos;
  ref_pos.push_back(temp_ref.x);
  ref_pos.push_back(temp_ref.y);
  ref_pos.push_back(temp_ref.z);
  ref_pos.push_back(temp_ref.yaw);
  return ref_pos;
}

std::vector<double> ReflexxesGenerator::getRefVel()
{
  boost::lock_guard<boost::mutex> lock(pp_ref_mutex_);
  std::vector<double> ref_vel;
  ref_vel.push_back(temp_ref.vx);
  ref_vel.push_back(temp_ref.vy);
  ref_vel.push_back(temp_ref.vz);
  ref_vel.push_back(temp_ref.yaw_rate);
  return ref_vel;
}

std::vector<double> ReflexxesGenerator::getRefAcc()
{
  boost::lock_guard<boost::mutex> lock(pp_ref_mutex_);
  std::vector<double> ref_acc;
  ref_acc.push_back(temp_ref.afx);
  ref_acc.push_back(temp_ref.afy);
  ref_acc.push_back(temp_ref.afz);
  ref_acc.push_back(0);
  return ref_acc;
}

void ReflexxesGenerator::ResetLimit(const GeneratorType& _type)
{
  switch (_type)
  {
    case kTakeOff:
    {
      // position
      IP->MaxVelocityVector->VecData[0] = pp_takeoff_maxVel_xy;
      IP->MaxVelocityVector->VecData[1] = pp_takeoff_maxVel_xy;
      IP->MaxVelocityVector->VecData[2] = pp_takeoff_maxVel_z;

      IP->MaxAccelerationVector->VecData[0] = pp_takeoff_maxAcc_xy;
      IP->MaxAccelerationVector->VecData[1] = pp_takeoff_maxAcc_xy;
      IP->MaxAccelerationVector->VecData[2] = pp_takeoff_maxAcc_z;

      IP->MaxJerkVector->VecData[0] = pp_takeoff_maxJerk_xy;
      IP->MaxJerkVector->VecData[1] = pp_takeoff_maxJerk_xy;
      IP->MaxJerkVector->VecData[2] = pp_takeoff_maxJerk_z;

      // heading
      reflexxes_heading_input_ptr_->MaxVelocityVector->VecData[0] = pp_takeoff_maxVel_c;
      reflexxes_heading_input_ptr_->MaxAccelerationVector->VecData[0] = pp_takeoff_maxAcc_c;
      reflexxes_heading_input_ptr_->MaxJerkVector->VecData[0] = pp_takeoff_maxJerk_c;

      break;
    }
    case kLand:
    {
      // position
      IP->MaxVelocityVector->VecData[0] = pp_land_maxVel_xy;
      IP->MaxVelocityVector->VecData[1] = pp_land_maxVel_xy;
      IP->MaxVelocityVector->VecData[2] = pp_land_maxVel_z;

      IP->MaxAccelerationVector->VecData[0] = pp_land_maxAcc_xy;
      IP->MaxAccelerationVector->VecData[1] = pp_land_maxAcc_xy;
      IP->MaxAccelerationVector->VecData[2] = pp_land_maxAcc_z;

      IP->MaxJerkVector->VecData[0] = pp_land_maxJerk_xy;
      IP->MaxJerkVector->VecData[1] = pp_land_maxJerk_xy;
      IP->MaxJerkVector->VecData[2] = pp_land_maxJerk_z;

      // heading
      reflexxes_heading_input_ptr_->MaxVelocityVector->VecData[0] = pp_land_maxVel_c;
      reflexxes_heading_input_ptr_->MaxAccelerationVector->VecData[0] = pp_land_maxAcc_c;
      reflexxes_heading_input_ptr_->MaxJerkVector->VecData[0] = pp_land_maxJerk_c;

      break;
    }
    case kGps:
    {
      // position
      IP->MaxVelocityVector->VecData[0] = pp_fly_maxVel_xy;
      IP->MaxVelocityVector->VecData[1] = pp_fly_maxVel_xy;
      IP->MaxVelocityVector->VecData[2] = pp_fly_maxVel_z;

      IP->MaxAccelerationVector->VecData[0] = pp_fly_maxAcc_xy;
      IP->MaxAccelerationVector->VecData[1] = pp_fly_maxAcc_xy;
      IP->MaxAccelerationVector->VecData[2] = pp_fly_maxAcc_z;

      IP->MaxJerkVector->VecData[0] = pp_fly_maxJerk_xy;
      IP->MaxJerkVector->VecData[1] = pp_fly_maxJerk_xy;
      IP->MaxJerkVector->VecData[2] = pp_fly_maxJerk_z;

      // heading
      reflexxes_heading_input_ptr_->MaxVelocityVector->VecData[0] = pp_fly_maxVel_c;
      reflexxes_heading_input_ptr_->MaxAccelerationVector->VecData[0] = pp_fly_maxAcc_c;
      reflexxes_heading_input_ptr_->MaxJerkVector->VecData[0] = pp_fly_maxJerk_c;
      break;
    }
    default:
    {
      break;
    }
  }

  IP->SelectionVector->VecData[0] = true;
  IP->SelectionVector->VecData[1] = true;
  IP->SelectionVector->VecData[2] = true;

  reflexxes_heading_input_ptr_->SelectionVector->VecData[0] = true;
}

void ReflexxesGenerator::Reset(const GeneratorType& _type)
{
  is_initialized_ = false;
  reflexxes_status_ = 0;  // byc
  ResetLimit(_type);
}

int ReflexxesGenerator::GetReflexxesStatus() const
{
  return reflexxes_status_;
}

void ReflexxesGenerator::SetCurrentPosition(const std::array<double, 2>& position)
{
  IP->CurrentPositionVector->VecData[0] = position[0];
  IP->CurrentPositionVector->VecData[1] = position[1];
}

}  // namespace nus_reference_generator
