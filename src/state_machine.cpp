// Copyright 2016-2017 Yuchao Hu
/*
 *  state_machine.cc
 *
 *  Created on: Sep 11, 2016
 *      Author: Yuchao Hu
 */

#include "nus_reference_generator/state_machine.h"
#include <std_msgs/Bool.h>
#include <stdio.h>
#include <iostream>
#include <limits>
#include <string>
#include "mavros/CommandHome.h"
#include "nus_msgs/PerchingPoint.h"
#include "nus_msgs/RegionOfInterest.h"

namespace nus_reference_generator
{
StateMachine::StateMachine(ros::NodeHandle n, const std::shared_ptr<MavlinkRos>& _mavlink_ros_ptr,
                           const std::shared_ptr<UavState>& _uav_state_ptr,
                           const std::shared_ptr<Navigator>& _navigator_ptr)
  : mavlink_ros_ptr_(_mavlink_ros_ptr)
  , uav_state_ptr_(_uav_state_ptr)
  , navigator_ptr_(_navigator_ptr)
  , enable_geofencing_(false)
{
  int _self_system_id;
  n.param("self_system_id", _self_system_id, 1);
  self_system_id_ = _self_system_id;
  n.param("enable_geofencing", enable_geofencing_, false);

  vehicle_cmd_sub_ = n.subscribe<mavros::Cmd>("/mavros/cmd/vehicle_cmd", 10, &StateMachine::VehicleCmdCallback, this);
  response_sub_ = n.subscribe<nus_reference_generator::response>("vehicle_action_response", 10,
                                                                 &StateMachine::ResponseCallback, this);
  task_manager_sub_ = n.subscribe<nus_msgs::StateStamped>("/nus_task_manager/current_target", 10,
                                                              &StateMachine::TaskManagerCallback, this);

  action_pub_ = n.advertise<nus_reference_generator::action>("vehicle_action", 10);
  task_manager_start_client_ = n.serviceClient<nus_msgs::StartTaskManager>("/nus_task_manager/start_task_manager");
  task_manager_perching_point_pub_ = n.advertise<nus_msgs::PerchingPoint>("/nus_task_manager/perching_point", 10);
  roi_pub_ = n.advertise<nus_msgs::RegionOfInterest>("region_of_interest", 10);
  
  // Set loop function with ROS timer
  main_loop_timer_ = n.createTimer(ros::Duration(0.02), &StateMachine::Loop, this);

  mavros_set_home_client_ = n.serviceClient<mavros::CommandHome>("/mavros/cmd/set_home");
  gps_to_local_position_server_ = n.advertiseService("gps_to_local_position", &StateMachine::Gps2LocalPosition, this);
  local_position_to_gps_server_ = n.advertiseService("local_position_to_gps", &StateMachine::LocalPosition2Gps, this);

  task_state_ = kUninit;
  keep_running_ = false;
}

StateMachine::~StateMachine()
{
  Stop();
  ROS_INFO("state machine class object is killed");
}

void StateMachine::Start()
{
  if (!keep_running_)
  {
    keep_running_ = true;
  }
}

void StateMachine::Stop()
{
  if (keep_running_)
  {
    keep_running_ = false;
  }
}

void StateMachine::Update()
{
  // Geofencing();
  // uav_state = uav_state_ptr_->getUAVState();
}

void StateMachine::ResponseCallback(const nus_reference_generator::response::ConstPtr& _msg_ptr)
{
  try
  {
    task_state_mutex_.lock();
    if (_msg_ptr->behavior_finished)
    {
      switch (_msg_ptr->cur_behavior)
      {
        case TAKEOFF:
        {
          if (kTakingOff == task_state_)
          {
            task_state_ = kHovering;
            ROS_INFO("[nus_reference_generator::StateMachine(Response)] TakingOff -> Hovering");
          }
          else if (kAutoTakingOff == task_state_)
          {
            // task_state_ = kAutoHovering;
            // ROS_INFO("[nus_reference_generator::StateMachine(Response)] AutoTakingOff -> AutoHovering");
            task_state_ = kAutoNavigating;
            ROS_INFO("[nus_reference_generator::StateMachine(Response)] AutoTakingOff finished, transit to AutoNavigating");
          }
          else
          {
            ROS_WARN("[nus_reference_generator::StateMachine(Response)] navigator reported TAKEOFF finished, but current state "
                     "is neither TakingOff nor AutoTakingOff");
          }
          break;
        }
        case NAVIGATE_TO:
        {
          if (kNavigating == task_state_)
          {
            task_state_ = kHovering;
            ROS_INFO("[nus_reference_generator::StateMachine(Response)] Navigating -> Hovering");
          }
          else if (kAutoNavigating == task_state_)
          {
            task_state_ = kAutoHovering;
            ROS_INFO("[nus_reference_generator::StateMachine(Response)] AutoNavigating, 1 waypoint completed...");
          }
          else
          {
            ROS_WARN("[nus_reference_generator::StateMachine(Response)] navigator reported NAVIGATE_TO finished, but current "
                     "state is neither Navigating nor AutoNavigating");
          }
          break;
        }
        case LAND:
        {
          if (kLanding == task_state_)
          {
            task_state_ = kLanded;
            ROS_INFO("[nus_reference_generator::StateMachine(Response)] Landing -> Landed");
          }
          else if (kAutoLanding == task_state_)
          {
            task_state_ = kAutoLanded;
            ROS_INFO("[nus_reference_generator::StateMachine(Response)] AutoLanded");
          }
          else
          {
            ROS_WARN("[nus_reference_generator::StateMachine(Response)] navigator reported LAND finished, but current state is "
                     "neither Landing nor AutoLanding");
          }
          break;
        }
        default:
        {
          break;
        }
      }
    }
    task_state_mutex_.unlock();
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("StateMachine::ResponseCallback");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

// \fn taskManagerCb(const nus_task_manager::taskmanager_output::ConstPtr& msg)
/*
 * get the local_target from task_manager
 */
void StateMachine::TaskManagerCallback(const nus_msgs::StateStamped::ConstPtr& _msg_ptr)
{
  try
  {
    task_state_mutex_.lock();
    switch (task_state_)
    {
      case kAutoReady:
      { 
        // check the takeoff height
        if (_msg_ptr->pose.position.z > 0.2)
        {
          // task_state_ = kAutoTakeOff;
          // ROS_INFO("[nus_reference_generator::StateMachine(Task)] AutoReady -> AutoTakeOff");
          task_state_ = kAutoTakingOff;
          ROS_INFO("[nus_reference_generator::StateMachine(Task)] kAutoReady -> AutoTakingOff");
          // FIXME: use service instead of publisher
          nus_reference_generator::action cur_action;
          cur_action.behavior = TAKEOFF;
          cur_action.frameid = 0;  // global frame
          cur_action.guid_signal_source = 0;
          cur_action.params.clear();
          cur_action.params.push_back(-_msg_ptr->pose.position.z);
          action_pub_.publish(cur_action);
        }
        break;
      }
      case kAutoHovering:
      case kAutoNavigating:
      {
        if (_msg_ptr->pose.position.z == 0)
        {
          // From Navigating to Land, this case is when no reach check from reflexxes
          task_state_ = kAutoLanding;
          ROS_INFO("[nus_reference_generator::StateMachine(Task)] kAutoNavigating -> AutoLanding");
          // FIXME: use service instead of publisher
          nus_reference_generator::action cur_action;
          cur_action.behavior = LAND;
          cur_action.frameid = 0;
          cur_action.guid_signal_source = 0;
          cur_action.params.clear();

          // position
          cur_action.params.push_back(_msg_ptr->pose.position.x);
          cur_action.params.push_back(-_msg_ptr->pose.position.y);
          cur_action.params.push_back(-_msg_ptr->pose.position.z);

          // orientation
          tf::Quaternion quat;
          tf::quaternionMsgToTF(_msg_ptr->pose.orientation, quat);
          tf::Matrix3x3 orientation_matrix(quat);
          double roll, pitch, yaw;
          orientation_matrix.getRPY(roll, pitch, yaw);
          cur_action.params.push_back(-yaw);

          // velocity target
          cur_action.params.push_back(_msg_ptr->velocity.linear.x);
          cur_action.params.push_back(-_msg_ptr->velocity.linear.y);
          cur_action.params.push_back(-_msg_ptr->velocity.linear.z);

          action_pub_.publish(cur_action);
        }
        else
        {
          // In navigation, keep giving current action to navigator
          if (kAutoHovering == task_state_)
          {
            task_state_ = kAutoNavigating;
            ROS_INFO("[nus_reference_generator::StateMachine(Task)] AutoHovering -> AutoNavigating");
          }
          // FIXME: use service instead of publisher
          nus_reference_generator::action cur_action;
          cur_action.behavior = NAVIGATE_TO;
          cur_action.frameid = 0;  // global frame
          cur_action.guid_signal_source = 0;
          cur_action.params.clear();

          // Get current target from task manager; Each autonomous plan should have its own task manager
          // Each time a new local target found, publish it to lower level reflexxes generator

          // position
          cur_action.params.push_back(_msg_ptr->pose.position.x);
          cur_action.params.push_back(-_msg_ptr->pose.position.y);
          cur_action.params.push_back(-_msg_ptr->pose.position.z);

          // orientation
          tf::Quaternion quat;
          tf::quaternionMsgToTF(_msg_ptr->pose.orientation, quat);
          tf::Matrix3x3 orientation_matrix(quat);
          double roll, pitch, yaw;
          orientation_matrix.getRPY(roll, pitch, yaw);
          cur_action.params.push_back(-yaw);

          // velocity target
          cur_action.params.push_back(_msg_ptr->velocity.linear.x);
          cur_action.params.push_back(-_msg_ptr->velocity.linear.y);
          cur_action.params.push_back(-_msg_ptr->velocity.linear.z);
          
          // acceleartion target
          cur_action.params.push_back(_msg_ptr->acceleration.linear.x);
          cur_action.params.push_back(-_msg_ptr->acceleration.linear.y);
          cur_action.params.push_back(-_msg_ptr->acceleration.linear.z);
         

          action_pub_.publish(cur_action);
        }
        break;
      }
      case kNavigating:
      {
#if 0
        // FIXME: use service instead of publisher
        nus_reference_generator::action cur_action;
        cur_action.behavior = NAVIGATE_TO;
        cur_action.frameid = 0;  // global frame
        cur_action.guid_signal_source = 0;
        cur_action.params.clear();
        // Get current target from task manager; Each autonomous plan should
        // have its own task manager
        // Each time a new local target found, publish it to lower level
        // reflexxes generator
        cur_action.params.push_back(_msg_ptr->x_ref);
        cur_action.params.push_back(_msg_ptr->y_ref);
        cur_action.params.push_back(_msg_ptr->z_ref);
        // Jinqiang: fixe navigation height to takeoff_height
        // cur_action.params.push_back(-takeoff_height);
        cur_action.params.push_back(_msg_ptr->c_ref);
        action_pub_.publish(cur_action);
#endif
        break;
      }
      default:
      {
        break;
      }
    }
    task_state_mutex_.unlock();
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("[nus_reference_generator::StateMachine::TaskManagerCallback]");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

void StateMachine::Loop(const ros::TimerEvent& event)
{
  if(keep_running_ && ros::ok())
  {
    Update();
  }
}

void StateMachine::VehicleCmdCallback(const mavros::Cmd::ConstPtr& _msg_ptr)
{

  try
  {
    if (self_system_id_ != _msg_ptr->target_system)
      return;
//    ROS_INFO_STREAM("[nus_reference_generator::StateMachine] command received "
//                    << static_cast<int>(_msg_ptr->param1 + 0.5));
    if (1 == _msg_ptr->command)
    {
      // the command is for ros system
      switch (static_cast<int>(_msg_ptr->param1 + 0.5))
      {
        case 0:  // TAKEOFF
        {
          break;
        }
        case 1:  // LAND
        {
          task_state_mutex_.lock();
          if (kLand != task_state_ && kLanding != task_state_ && kLanded != task_state_)
          {
            std::string info_string("[nus_reference_generator::StateMachine] land command accepted!");
            ROS_INFO_STREAM(info_string);
            mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

            // FIXME: maybe no need to shutdown task manager first
            // call start task manager
            nus_msgs::StartTaskManager task_manager_srv;
            task_manager_srv.request.start = false; // shutdown task_manager
            if (task_manager_start_client_.call(task_manager_srv))
            {
              if (task_manager_srv.response.started)
              {
                std::string info_string("[nus_reference_generator::StateMachine] Task manager started!");
                ROS_INFO_STREAM(info_string);
                mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);
              }
              else
              {
                std::string info_string("[nus_reference_generator::StateMachine] Task manager stopped!");
                ROS_WARN_STREAM(info_string);
                mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_WARNING);
              }
            }
            else
            {
              std::string info_string("[nus_reference_generator::StateMachine] Failed to call service "
                                      "start_task_manager!");
              ROS_WARN_STREAM(info_string);
              mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_WARNING);
            }

            task_state_ = kLand;
            ROS_INFO("[nus_reference_generator::StateMachine] Land");
            nus_reference_generator::action cur_action;
            cur_action.behavior = LAND;
            cur_action.frameid = 0;
            action_pub_.publish(cur_action);
            task_state_ = kLanding;
            ROS_INFO("[nus_reference_generator::StateMachine] Land -> Landing");
          }
          else
          {
            std::string warn_string("[nus_reference_generator::StateMachine] land command rejected!");
            ROS_WARN_STREAM(warn_string);
            mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
          }
          task_state_mutex_.unlock();
          break;
        }
        case 2:  // HFLY
        case 3:  // TEST
        case 4:  // RETURN_HOME
        case 5:  // General
        {
          break;
        }
        case 7:  // Reset reference (engine: 0)
        {
          task_state_mutex_.lock();
          if (kUninit == task_state_ || kReady == task_state_)
          {
            std::string info_string("[nus_reference_generator::StateMachine] Engine 0 reset command accepted!");
            ROS_INFO_STREAM(info_string);
            mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

            // call set home in mavros
            mavros::CommandHome mavros_set_home_srv;
            mavros_set_home_srv.request.current_gps = true;
            if (mavros_set_home_client_.call(mavros_set_home_srv))
            {
              if (mavros_set_home_srv.response.success)
              {
                std::string info_string("[nus_reference_generator::StateMachine] Set home succeed!");
                ROS_INFO_STREAM(info_string);
                mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);
              }
              else
              {
                std::string info_string("[nus_reference_generator::StateMachine] Set home failed!");
                ROS_WARN_STREAM(info_string);
                mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_WARNING);
              }
            }
            else
            {
              std::string info_string("[nus_reference_generator::StateMachine] Failed to call service "
                                      "mavros_set_home!");
              ROS_WARN_STREAM(info_string);
              mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_WARNING);
            }

            // FIXME: check if measurement is received in navigator
            task_state_ = kReady;
            ROS_INFO("[nus_reference_generator::StateMachine] Uninit -> Ready");
            // FIXME: use service instead of publisher
            nus_reference_generator::action cur_action;
            cur_action.behavior = IDLE;
            cur_action.params.clear();
            cur_action.params.push_back(_msg_ptr->param2);
            cur_action.params.push_back(_msg_ptr->param3);
            cur_action.params.push_back(_msg_ptr->param4);
            cur_action.params.push_back(_msg_ptr->param5);
            action_pub_.publish(cur_action);
          }
          else
          {
            std::string warn_string("[nus_reference_generator::StateMachine] Engine 0 reset command rejected!");
            ROS_WARN_STREAM(warn_string);
            mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
          }
          task_state_mutex_.unlock();
          break;
        }
        case 8:  // Navigate to a waypoint
        {
          break;
        }
        case 14:  // Plan cmd
        {
          HandlePlanCmd(_msg_ptr);
          break;
        }
        default:
        {
          break;
        }
      }
    }
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("[nus_reference_generator::StateMachine::VehicleCmdCallback]");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

void StateMachine::HandlePlanCmd(const mavros::Cmd::ConstPtr& _msg_ptr)
{
  int cmd = _msg_ptr->param2 + 0.5;
  task_state_mutex_.lock();
  switch (cmd)
  {
    case kPlanAuto:
    {
      if (kReady == task_state_)
      {
        std::string info_string("[nus_reference_generator::StateMachine] Plan 0 auto command accepted!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

        // call start task manager
        nus_msgs::StartTaskManager start_task_manager_srv;
        start_task_manager_srv.request.start = true;
        if (task_manager_start_client_.call(start_task_manager_srv))
        {
          if (start_task_manager_srv.response.started)
          {
            std::string info_string("[nus_reference_generator::StateMachine] Start Task manager.");
            ROS_INFO_STREAM(info_string);
            mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);
          }
          else
          {
            std::string info_string("[nus_reference_generator::StateMachine] Stop Task manager.");
            ROS_WARN_STREAM(info_string);
            mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_WARNING);
          }
        }
        else
        {
          std::string info_string("[nus_reference_generator::StateMachine] Failed to call service "
                                  "start_task_manager!");
          ROS_WARN_STREAM(info_string);
          mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_WARNING);
        }

        task_state_ = kAutoReady;
        ROS_INFO("[nus_reference_generator::StateMachine] Ready -> AutoReady");
      }
      else
      {
        std::string warn_string("[nus_reference_generator::StateMachine] plan 0 auto command rejected!");
        ROS_WARN_STREAM(warn_string);
        mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
      }
      break;
    }
    case kPlanTakeOff:
    {
      if (kReady == task_state_ && 0.0 != _msg_ptr->param3)
      {  // safety check
        std::string info_string("[nus_reference_generator::StateMachine] plan 1 takeoff command accepted!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

        UAV_STATE uav_state = uav_state_ptr_->getUAVState();

        task_state_ = kTakeOff;
        ROS_INFO("[nus_reference_generator::StateMachine] Ready -> TakeOff");
        // FIXME: use service instead of publisher
        const double height = _msg_ptr->param3;
        nus_reference_generator::action cur_action;
        cur_action.behavior = TAKEOFF;
        cur_action.frameid = 0;             // global frame
        cur_action.guid_signal_source = 0;  // Action.signal_source;
        cur_action.params.clear();
        cur_action.params.push_back(-height + uav_state.z_pos);  // UP -> DOWN frame
        action_pub_.publish(cur_action);
        task_state_ = kTakingOff;
        ROS_INFO("[nus_reference_generator::StateMachine] TakeOff -> TakingOff");
      }
      else
      {
        std::string warn_string("[nus_reference_generator::StateMachine] plan 1 takeoff command rejected!");
        ROS_WARN_STREAM(warn_string);
        mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
      }
      break;
    }
    case kPlanNavigate:
    {
      if ((kHovering == task_state_ || kNavigating == task_state_) && 0.0 != _msg_ptr->param3 &&
          0.0 != _msg_ptr->param4 && 0.0 != _msg_ptr->param5)
      {  // safety check
        std::string info_string("[nus_reference_generator::StateMachine] plan 2 navigate command accepted!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

        UAV_STATE uav_state = uav_state_ptr_->getUAVState();

        // FIXME: use service instead of publisher
        const double x_pos = _msg_ptr->param3;
        const double y_pos = _msg_ptr->param4;
        const double z_pos = _msg_ptr->param5;
        nus_reference_generator::action cur_action;
        cur_action.behavior = NAVIGATE_TO;
        cur_action.frameid = 0;  // global frame
        cur_action.guid_signal_source = 0;
        cur_action.params.clear();
        cur_action.params.push_back(x_pos);
        cur_action.params.push_back(y_pos);
        cur_action.params.push_back(z_pos);
        cur_action.params.push_back(uav_state.c_pos);
        action_pub_.publish(cur_action);

        task_state_ = kNavigating;
        ROS_INFO("[nus_reference_generator::StateMachine] Hovering -> Navigating");
      }
      else
      {
        std::string warn_string("[nus_reference_generator::StateMachine] plan 2 navigate command rejected!");
        ROS_WARN_STREAM(warn_string);
        mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
      }
      break;
    }
    case kPlanLand:
    case kPlanGPSLand:
    {
      if (kLand != task_state_ && kLanding != task_state_ && kLanded != task_state_)
      {
        std::string info_string("[nus_reference_generator::StateMachine] plan 3 land command accepted!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

        // FIXME: maybe no need to shutdown task manager first
        // call start task manager
        nus_msgs::StartTaskManager task_manager_srv;
        task_manager_srv.request.start = false; // shutdown task_manager
        if (task_manager_start_client_.call(task_manager_srv))
        {
          if (task_manager_srv.response.started)
          {
            std::string info_string("[nus_reference_generator::StateMachine] Task manager started!");
            ROS_INFO_STREAM(info_string);
            mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);
          }
          else
          {
            std::string info_string("[nus_reference_generator::StateMachine] Task manager stopped!");
            ROS_WARN_STREAM(info_string);
            mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_WARNING);
          }
        }
        else
        {
          std::string info_string("[nus_reference_generator::StateMachine] Failed to call service "
                                  "start_task_manager!");
          ROS_WARN_STREAM(info_string);
          mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_WARNING);
        }

        task_state_ = kLand;
        ROS_INFO("[nus_reference_generator::StateMachine] Land");
        nus_reference_generator::action cur_action;
        cur_action.behavior = LAND;
        cur_action.frameid = 0;
        action_pub_.publish(cur_action);
        task_state_ = kLanding;
        ROS_INFO("[nus_reference_generator::StateMachine] Land -> Landing");
      }
      else
      {
        std::string warn_string("[nus_reference_generator::StateMachine] plan 3 land command rejected!");
        ROS_WARN_STREAM(warn_string);
        mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
      }
      break;
    }
    case kPlanForward:
    {
      if (kHovering == task_state_)
      {
        std::string info_string("[nus_reference_generator::StateMachine] plan 4 forward command accepted!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

        UAV_STATE uav_state = uav_state_ptr_->getUAVState();

        // FIXME: use service instead of publisher
        const double x_diff = _msg_ptr->param3;
        nus_reference_generator::action cur_action;
        cur_action.behavior = NAVIGATE_TO;
        cur_action.frameid = 0;  // global frame
        cur_action.guid_signal_source = 0;
        cur_action.params.clear();
        cur_action.params.push_back(uav_state.x_pos + x_diff);
        cur_action.params.push_back(uav_state.y_pos);
        cur_action.params.push_back(uav_state.z_pos);
        cur_action.params.push_back(uav_state.c_pos);
        action_pub_.publish(cur_action);

        task_state_ = kNavigating;
        ROS_INFO("[nus_reference_generator::StateMachine] Hovering -> Navigating");
      }
      else
      {
        std::string warn_string("[nus_reference_generator::StateMachine] plan 4 forward command rejected!");
        ROS_WARN_STREAM(warn_string);
        mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
      }
      break;
    }
    case kPlanBack:
    {
      if (kHovering == task_state_)
      {
        std::string info_string("[nus_reference_generator::StateMachine] plan 5 back command accepted!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

        UAV_STATE uav_state = uav_state_ptr_->getUAVState();

        // FIXME: use service instead of publisher
        const double x_diff = _msg_ptr->param3;
        nus_reference_generator::action cur_action;
        cur_action.behavior = NAVIGATE_TO;
        cur_action.frameid = 0;  // global frame
        cur_action.guid_signal_source = 0;
        cur_action.params.clear();
        cur_action.params.push_back(uav_state.x_pos - x_diff);
        cur_action.params.push_back(uav_state.y_pos);
        cur_action.params.push_back(uav_state.z_pos);
        cur_action.params.push_back(uav_state.c_pos);
        action_pub_.publish(cur_action);

        task_state_ = kNavigating;
        ROS_INFO("[nus_reference_generator::StateMachine] Hovering -> Navigating");
      }
      else
      {
        std::string warn_string("[nus_reference_generator::StateMachine] plan 5 back command rejected!");
        ROS_WARN_STREAM(warn_string);
        mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
      }
      break;
    }
    case kPlanLeft:
    {
      if (kHovering == task_state_)
      {
        std::string info_string("[nus_reference_generator::StateMachine] plan 6 left command accepted!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

        UAV_STATE uav_state = uav_state_ptr_->getUAVState();

        // FIXME: use service instead of publisher
        const double y_diff = _msg_ptr->param3;
        nus_reference_generator::action cur_action;
        cur_action.behavior = NAVIGATE_TO;
        cur_action.frameid = 0;  // global frame
        cur_action.guid_signal_source = 0;
        cur_action.params.clear();
        cur_action.params.push_back(uav_state.x_pos);
        cur_action.params.push_back(uav_state.y_pos - y_diff);
        cur_action.params.push_back(uav_state.z_pos);
        cur_action.params.push_back(uav_state.c_pos);
        action_pub_.publish(cur_action);

        task_state_ = kNavigating;
        ROS_INFO("[nus_reference_generator::StateMachine] Hovering -> Navigating");
      }
      else
      {
        std::string warn_string("[nus_reference_generator::StateMachine] plan 6 left command rejected!");
        ROS_WARN_STREAM(warn_string);
        mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
      }
      break;
    }
    case kPlanRight:
    {
      if (kHovering == task_state_)
      {
        std::string info_string("[nus_reference_generator::StateMachine] plan 7 right command accepted!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

        UAV_STATE uav_state = uav_state_ptr_->getUAVState();

        // FIXME: use service instead of publisher
        const double y_diff = _msg_ptr->param3;
        nus_reference_generator::action cur_action;
        cur_action.behavior = NAVIGATE_TO;
        cur_action.frameid = 0;  // global frame
        cur_action.guid_signal_source = 0;
        cur_action.params.clear();
        cur_action.params.push_back(uav_state.x_pos);
        cur_action.params.push_back(uav_state.y_pos + y_diff);
        cur_action.params.push_back(uav_state.z_pos);
        cur_action.params.push_back(uav_state.c_pos);
        action_pub_.publish(cur_action);

        task_state_ = kNavigating;
        ROS_INFO("[nus_reference_generator::StateMachine] Hovering -> Navigating");
      }
      else
      {
        std::string warn_string("[nus_reference_generator::StateMachine] plan 7 right command rejected!");
        ROS_WARN_STREAM(warn_string);
        mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
      }
      break;
    }
    case kPlanSetROI:
    {
      // TODO(ronky): publish ROI
      nus_msgs::RegionOfInterest roi_msg;
      roi_msg.header.stamp = ros::Time::now();
      roi_msg.x_offset_ratio = _msg_ptr->param3;
      roi_msg.y_offset_ratio = _msg_ptr->param4;
      roi_msg.width_ratio = _msg_ptr->param5;
      roi_msg.height_ratio = _msg_ptr->param6;
      roi_pub_.publish(roi_msg);
      break;
    }
    case kPlanGPSTakeOff:
    {
      if (kReady == task_state_ && 0.0 != _msg_ptr->param3)
      {  // safety check
        std::string info_string("[nus_reference_generator::StateMachine] plan 11 gps takeoff command accepted!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

        task_state_ = kTakeOff;
        ROS_INFO("[nus_reference_generator::StateMachine] Ready -> TakeOff");
        // FIXME: use service instead of publisher
        UAV_STATE uav_state = uav_state_ptr_->getUAVState();
        GPS_COORDINATE gps_coordinate = uav_state_ptr_->getGPSCoordinate();
        const double target_gps_height = _msg_ptr->param3;
        nus_reference_generator::action cur_action;
        cur_action.behavior = TAKEOFF;
        cur_action.frameid = 0;             // global frame
        cur_action.guid_signal_source = 0;  // Action.signal_source;
        cur_action.params.clear();
        cur_action.params.push_back(uav_state.z_pos -
                                    (target_gps_height - gps_coordinate.altitude));  // UP -> DOWN frame
        action_pub_.publish(cur_action);
        task_state_ = kTakingOff;
        ROS_INFO("[nus_reference_generator::StateMachine] TakeOff -> TakingOff");
      }
      else
      {
        std::string warn_string("[nus_reference_generator::StateMachine] plan 11 gps takeoff command rejected!");
        ROS_WARN_STREAM(warn_string);
        mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
      }
      break;
    }
    case kPlanGPSNavigate:
    {
      if ((kHovering == task_state_ || kNavigating == task_state_) && 0.0 != _msg_ptr->param3 &&
          0.0 != _msg_ptr->param4 && 0.0 != _msg_ptr->param5)
      {  // safety check
        std::string info_string("[nus_reference_generator::StateMachine] plan 12 gps navigate command accepted!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

        const double target_gps_lat = _msg_ptr->param3;
        const double target_gps_lon = _msg_ptr->param4;
        const double target_gps_alt = _msg_ptr->param5;

        UAV_STATE uav_state = uav_state_ptr_->getUAVState();
        mavlink_set_position_target_local_ned_t uav_ref = navigator_ptr_->GetUAVReference();

        // convert to local position relative to home
        struct map_projection_reference_s home_gps_ref;
        float x_pos, y_pos;
        map_projection_init(&home_gps_ref, navigator_ptr_->GetHomeLatitude(), navigator_ptr_->GetHomeLongitude());
        map_projection_project(&home_gps_ref, target_gps_lat, target_gps_lon, &x_pos, &y_pos);

        {
          std::string info_string("[nus_reference_generator::StateMachine] gps -> local frame: north " +
                                  std::to_string(x_pos) + "m, east " + std::to_string(y_pos) + "m");
          ROS_INFO_STREAM(info_string);
          mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);
        }

        // FIXME: use service instead of publisher
        nus_reference_generator::action cur_action;
        cur_action.behavior = NAVIGATE_TO;
        cur_action.frameid = 0;  // global frame
        cur_action.guid_signal_source = 0;
        cur_action.params.clear();
        cur_action.params.push_back(x_pos + navigator_ptr_->get_home_x());  // convert to local position
                                                                            // relative to origin
        cur_action.params.push_back(y_pos + navigator_ptr_->get_home_y());
        cur_action.params.push_back(navigator_ptr_->get_home_z() -
                                    (target_gps_alt - navigator_ptr_->GetHomeAltitude()));
        cur_action.params.push_back(uav_ref.yaw);
        action_pub_.publish(cur_action);

        task_state_ = kNavigating;
        ROS_INFO("[nus_reference_generator::StateMachine] Hovering -> Navigating");
      }
      else
      {
        std::string warn_string("[nus_reference_generator::StateMachine] plan 12 gps navigate command rejected!");
        ROS_WARN_STREAM(warn_string);
        mavlink_ros_ptr_->disp_on_gcs(warn_string, MAV_SEVERITY_WARNING);
      }
      break;
    }
    case kPlanSetGPS:
    {
      const double& lat = _msg_ptr->param3;
      const double& lon = _msg_ptr->param4;
      const double& alt = _msg_ptr->param5;
      uav_state_ptr_->setGPSCoordinate(lat, lon, alt);
      {
        std::string info_string("[nus_reference_generator::StateMachine] set gps: lat " + std::to_string(lat) +
                                ", lon " + std::to_string(lon) + ", alt " + std::to_string(alt));
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);
      }
      break;
    }
    case kPlanSetTaskManagerPerchingPoint:
    {
      const double& lat = _msg_ptr->param3;
      const double& lon = _msg_ptr->param4;
      const double& alt = _msg_ptr->param5;
      const float& wall_normal = _msg_ptr->param6;
      const float& navigate_height = _msg_ptr->param7;

      GPS_COORDINATE gps_coordinate = uav_state_ptr_->getGPSCoordinate();
      // convert to local position relative to home
      struct map_projection_reference_s home_gps_ref;
      float x_pos, y_pos;
      map_projection_init(&home_gps_ref, gps_coordinate.latitude, gps_coordinate.longitude);
      map_projection_project(&home_gps_ref, lat, lon, &x_pos, &y_pos);

      // NE to NW frame
      nus_msgs::PerchingPoint msg;
      msg.pose.position.x = x_pos;
      msg.pose.position.y = -y_pos;
      msg.pose.position.z = alt;
      msg.pose.orientation.x = 0;
      msg.pose.orientation.y = 0;
      msg.pose.orientation.z = 0;
      msg.pose.orientation.w = 1;
      msg.wall_normal = wall_normal;
      msg.navigate_height = navigate_height;
      task_manager_perching_point_pub_.publish(msg);
      break;
    }
    default:
    {
      break;
    }
  }
  task_state_mutex_.unlock();
}

void StateMachine::Geofencing()
{
  UAV_STATE uav_state = uav_state_ptr_->getUAVState();
  GPS_COORDINATE uav_gps_coordinate = uav_state_ptr_->getGPSCoordinate();
  // TODO(ronky): check if out of range
  bool is_out_of_range = false;
  task_state_mutex_.lock();
  if (kLand != task_state_ && kLanding != task_state_ && kLanded != task_state_ && is_out_of_range)
  {
    std::string info_string("[nus_reference_generator::StateMachine] out of geofence, land!");
    ROS_INFO_STREAM(info_string);
    mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);

    // FIXME: maybe no need to shutdown task manager first
    // call start task manager
    nus_msgs::StartTaskManager task_manager_srv;
    task_manager_srv.request.start = false; // shutdown task_manager
    if (task_manager_start_client_.call(task_manager_srv))
    {
      if (task_manager_srv.response.started)
      {
        std::string info_string("[nus_reference_generator::StateMachine] Task manager started!");
        ROS_INFO_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_INFO);
      }
      else
      {
        std::string info_string("[nus_reference_generator::StateMachine] Task manager stopped!");
        ROS_WARN_STREAM(info_string);
        mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_WARNING);
      }
    }
    else
    {
      std::string info_string("[nus_reference_generator::StateMachine] Failed to call service "
                              "start_task_manager!");
      ROS_WARN_STREAM(info_string);
      mavlink_ros_ptr_->disp_on_gcs(info_string, MAV_SEVERITY_WARNING);
    }

    task_state_ = kLand;
    ROS_INFO("[nus_reference_generator::StateMachine] Land");
    nus_reference_generator::action cur_action;
    cur_action.behavior = LAND;
    cur_action.frameid = 0;
    action_pub_.publish(cur_action);
    task_state_ = kLanding;
    ROS_INFO("[nus_reference_generator::StateMachine] Land -> Landing");
  }
  task_state_mutex_.unlock();
}

bool StateMachine::Gps2LocalPosition(nus_msgs::Gps2LocalPosition::Request& req,
                                     nus_msgs::Gps2LocalPosition::Response& res)
{
  switch (task_state_)
  {
    case kUninit:
    case kReady:
    case kAutoReady:
    case kTakeOff:
    case kAutoTakeOff:
    case kTakingOff:
    case kAutoTakingOff:
    {
      res.success = false;
      return true;
    }
  }

  const double& lat = req.gps.latitude;
  const double& lon = req.gps.longitude;
  const double& alt = req.gps.altitude;
  struct map_projection_reference_s home_gps_ref;
  float x_pos, y_pos;
  map_projection_init(&home_gps_ref, navigator_ptr_->GetHomeLatitude(), navigator_ptr_->GetHomeLongitude());
  int rt = map_projection_project(&home_gps_ref, lat, lon, &x_pos, &y_pos);
  res.success = (0 == rt) ? true : false;
  res.local_position.x = x_pos;
  res.local_position.y = -y_pos;  // NWU -> NED
  res.local_position.z = alt - navigator_ptr_->GetHomeAltitude();

  return true;
}

bool StateMachine::LocalPosition2Gps(nus_msgs::LocalPosition2Gps::Request& req,
                                     nus_msgs::LocalPosition2Gps::Response& res)
{
  switch (task_state_)
  {
    case kUninit:
    case kReady:
    case kAutoReady:
    case kTakeOff:
    case kAutoTakeOff:
    case kTakingOff:
    case kAutoTakingOff:
    {
      res.success = false;
      return true;
    }
  }

  const double& x_pos = req.local_position.x;
  const double& y_pos = req.local_position.y;
  const double& z_pos = req.local_position.z;
  double lat, lon;
  struct map_projection_reference_s home_gps_ref;
  map_projection_init(&home_gps_ref, navigator_ptr_->GetHomeLatitude(), navigator_ptr_->GetHomeLongitude());
  int rt = map_projection_reproject(&home_gps_ref, x_pos, -y_pos, &lat, &lon);  // NWU -> NED
  res.success = (0 == rt) ? true : false;
  res.gps.latitude = lat;
  res.gps.longitude = lon;
  res.gps.altitude = z_pos + navigator_ptr_->GetHomeAltitude();

  return true;
}

}  // namespace nus_reference_generator
