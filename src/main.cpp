// Copyright 2015-2017 Yuchao Hu
/*
 *  main.cc
 *
 *  Created on: 2016
 *      Author: Yuchao Hu
 */

#include <ros/ros.h>

#include "nus_reference_generator/mission_control.h"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "nus_reference_generator");

  nus_reference_generator::MissionControl m;

  ros::MultiThreadedSpinner spinner(4); /* Use 4 threads */
  spinner.spin();


  return (0);
}
