// Copyright 2015-2017 Yuchao Hu
/*
 * uav_state.cc
 *
 *  Created on: Aug 25, 2015
 *      Author: li
 */

#include "nus_reference_generator/uav_state.h"
#include <boost/thread/lock_guard.hpp>
#include <vector>

namespace nus_reference_generator
{
UAV_STATE UavState::getUAVState()
{
  boost::lock_guard<boost::mutex> lock(state_mutex_);
  return state;
}

UavState::UavState(ros::NodeHandle n)
{
  updateTime = 0;

  boost::lock_guard<boost::mutex> lock(state_mutex_);
  memset(&state, 0, sizeof(state));
  memset(&gps, 0, sizeof(gps));
  memset(&mag, 0, sizeof(mag));
  memset(&ref, 0, sizeof(ref));
  pressed_status = 0;
  diff_count = 0;
  geo_ref_inited = false;

  bool enable_hitl = false;
  n.param("enable_hitl", enable_hitl, false);

  /*subscriber of uav info*/
  local_sub = n.subscribe("/mavros/position/local", 10, &UavState::local_sub_callback, this);
  compass_hdg_sub = n.subscribe("/mavros/global_position/compass_hdg", 10, &UavState::compass_hdg_sub_callback, this);
  global_gps_sub = n.subscribe("/mavros/global_position/global", 10, &UavState::global_gps_sub_callback, this);
  global_gps_vel_sub = n.subscribe("/mavros/global_position/gps_vel", 10, &UavState::global_gps_vel_sub_callback, this);
  imu_data_sub = n.subscribe("/mavros/imu/data", 10, &UavState::imu_data_sub_callback, this);
  imu_mag_sub = n.subscribe("/mavros/imu/mag", 10, &UavState::imu_mag_sub_callback, this);

  /* ROS parameter */
  // ros::NodeHandle private_nh("~");

  double initial_position_x, initial_position_y, initial_position_z;
  n.param("initial_position_x", initial_position_x, 0.0);
  n.param("initial_position_y", initial_position_y, 0.0);
  n.param("initial_position_z", initial_position_z, 0.0);
  // NWU -> NED
  state.x_pos = initial_position_x;
  state.y_pos = -initial_position_y;
  state.z_pos = -initial_position_z;

  printf("[UAV STATE] init position: %f, %f, %f\n", initial_position_x, initial_position_y, initial_position_z);
  std::cout << "[INIT] UAV state ready." << std::endl;
}

void UavState::setUAVState(std::vector<double> src)
{
  boost::lock_guard<boost::mutex> lock(state_mutex_);

  state.x_pos = src.at(0);
  state.y_pos = src.at(1);
  state.z_pos = src.at(2);
  state.c_pos = src.at(3);

  state.x_vel = src.at(4);
  state.y_vel = src.at(5);
  state.z_vel = src.at(6);
  state.c_vel = src.at(7);

  state.x_acc = src.at(8);
  state.y_acc = src.at(9);
  state.z_acc = src.at(10);
  state.c_acc = src.at(11);

  state.roll = src.at(12);
  state.pitch = src.at(13);
  state.yaw = src.at(14);

  updateTime = nus_reference_generator::getElapsedTime();
}

void UavState::setUAVState_xyz(double x, double y, double z)
{
  boost::lock_guard<boost::mutex> lock(state_mutex_);
  state.x_pos = x;
  state.y_pos = y;
  state.z_pos = z;

  updateTime = nus_reference_generator::getElapsedTime();
}

void UavState::setUAVState_abc(double roll, double pitch, double yaw)
{
  boost::lock_guard<boost::mutex> lock(state_mutex_);
  state.roll = roll;
  state.pitch = pitch;
  state.yaw = yaw;

  updateTime = nus_reference_generator::getElapsedTime();
}

void UavState::setUAVState_heading(double c)
{
  boost::lock_guard<boost::mutex> lock(state_mutex_);
  state.c_pos = DEG2RAD(c);
}

void UavState::setUAVState_vel_linear(double vx, double vy, double vz)
{
  boost::lock_guard<boost::mutex> lock(state_mutex_);
  state.x_vel = vx;
  state.y_vel = vy;
  state.z_vel = vz;
}

void UavState::setUAVState_vel_angular(double vroll, double vpitch, double vyaw)
{
  boost::lock_guard<boost::mutex> lock(state_mutex_);
  state.roll_vel = vroll;
  state.pitch_vel = vpitch;
  state.yaw_vel = vyaw;

  state.c_vel = vyaw;
}

void UavState::setUAVState_acc_linear(double ax, double ay, double az)
{
  boost::lock_guard<boost::mutex> lock(state_mutex_);
  state.x_acc = ax;
  state.y_acc = ay;
  state.z_acc = az;
}

void UavState::print_state()
{
  printf("[UAV STATE]###########################\n");
  printf("x is %lf, y is %lf, z is %lf, c is %lf\n", state.x_pos, state.y_pos, state.z_pos, state.c_pos);
/*
  printf("vx is %lf, vy is %lf, vz is %lf, vc is %lf\n", state.x_vel, state.y_vel, state.z_vel, state.c_vel);
  printf("acc x is %lf, acc y is %lf, acc z is %lf\n", state.x_acc, state.y_acc, state.z_acc);
  printf("roll is %lf, pitch is %lf, yaw is %lf\n", state.roll, state.pitch, state.yaw);
  printf("roll speed is %lf, pitch speed is %lf, yaw speed is %lf\n", state.roll_vel, state.pitch_vel, state.yaw_vel);
  printf("lat is %lf, lon is %lf, alt is %lf\n", gps.latitude, gps.longitude, gps.altitude);
  printf("###########################\n\n");
*/
}

GPS_COORDINATE UavState::getGPSCoordinate()
{
  boost::lock_guard<boost::mutex> lock(gps_mutex_);
  return gps;
}

void UavState::setGPSCoordinate(double latitude, double longitude, double altitude)
{
  boost::lock_guard<boost::mutex> lock(gps_mutex_);
  gps.latitude = latitude;
  gps.longitude = longitude;
  gps.altitude = altitude;
}

void UavState::setGPSCovStatus(const sensor_msgs::NavSatFix::ConstPtr& msg)
{
  try
  {
    boost::lock_guard<boost::mutex> lock(gps_mutex_);
    for (int i = 0; i < 9; ++i)
    {
      gps.position_cov[i] = msg->position_covariance.at(i);
    }
    gps.service = msg->status.service;
    gps.status = msg->status.status;
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("UavState::setGPSCovStatus");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

MAGNETICFIELD UavState::getMag()
{
  boost::lock_guard<boost::mutex> lock(mag_mutex_);
  return mag;
}
void UavState::setMag(double x, double y, double z)
{
  boost::lock_guard<boost::mutex> lock(mag_mutex_);
  mag.x = x;
  mag.y = y;
  mag.z = z;
}

void UavState::setMagCov(const sensor_msgs::MagneticField::ConstPtr& msg)
{
  try
  {
    boost::lock_guard<boost::mutex> lock(mag_mutex_);
    for (int i = 0; i < 9; ++i)
    {
      mag.mag_cov[i] = msg.get()->magnetic_field_covariance.at(i);
    }
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("UavState::setMagCov");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

// ------------------------------ callback functions for mavlink communication
// ----------------
void UavState::angleTF(double x, double y, double z, double w, double& r1, double& p1, double& y1)
{
  tf::Quaternion q(x, y, z, w);
  tf::Matrix3x3 m(q);
  double roll, pitch, yaw;
  m.getRPY(roll, pitch, yaw);
  r1 = roll;
  p1 = -pitch;
  y1 = -yaw;
}

void UavState::local_sub_callback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
  try
  {
    double x, y, z;
    linearCoordinateTF(msg.get()->pose.position.x, msg.get()->pose.position.y, msg.get()->pose.position.z, x, y, z);
    setUAVState_xyz(x, y, z);

    double roll, pitch, yaw;
    angleTF(msg.get()->pose.orientation.x, msg.get()->pose.orientation.y, msg.get()->pose.orientation.z,
            msg.get()->pose.orientation.w, roll, pitch, yaw);
    setUAVState_abc(roll, pitch, yaw);
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("UavState::local_sub_callback");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

void UavState::compass_hdg_sub_callback(const std_msgs::Float64::ConstPtr& msg)
{
  try
  {
    setUAVState_heading(msg.get()->data);
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("UavState::compass_hdg_sub_callback");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

void UavState::global_gps_vel_sub_callback(const geometry_msgs::Vector3Stamped::ConstPtr& msg)
{
  try
  {
    double vx, vy, vz;
    // linearCoordinateTF(msg.get()->vector.x, msg.get()->vector.y,
    // msg.get()->vector.z,
    //            vx,vy,vz);
    vx = msg.get()->vector.x;
    vy = msg.get()->vector.y;
    vz = msg.get()->vector.z;
    setUAVState_vel_linear(vx, vy, vz);
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("UavState::global_gps_vel_sub_callback");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

void UavState::global_gps_sub_callback(const sensor_msgs::NavSatFix::ConstPtr& msg)
{
  try
  {
    setGPSCoordinate(msg.get()->latitude, msg.get()->longitude, msg.get()->altitude);
    if (!geo_ref_inited)
    {
      static int init_counter = 0;  // avoid multi-threading sheduling problem
      init_counter++;
      if (init_counter > 10)
      {
        boost::lock_guard<boost::mutex> lock(gps_mutex_);
        boost::lock_guard<boost::mutex> lock2(state_mutex_);
        /* remap the local position to geo ref */
        struct map_projection_reference_s temp_ref;
        map_projection_init(&temp_ref, msg.get()->latitude, msg.get()->longitude);
        double temp_lat, temp_lon;
        map_projection_reproject(&temp_ref, -state.x_pos, -state.y_pos, &temp_lat, &temp_lon);
        map_projection_init(&ref, temp_lat, temp_lon);
        ref_alt = msg.get()->altitude + state.z_pos;
        geo_ref_inited = true;
        printf("[debug] ref init finished!ref lat is %lf, ref lon is %lf\n", ref.lat_rad, ref.lon_rad);
      }
    }
    setGPSCovStatus(msg);
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("UavState::global_gps_sub_callback");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

map_projection_reference_s UavState::getGPSref()
{
  boost::lock_guard<boost::mutex> lock(gps_mutex_);
  return ref;
}

void UavState::imu_data_sub_callback(const sensor_msgs::Imu::ConstPtr& msg)
{
  try
  {
    double ax = msg.get()->linear_acceleration.x;
    double ay = -1 * msg.get()->linear_acceleration.y;
    double az = -1 * msg.get()->linear_acceleration.z;
    setUAVState_acc_linear(ax, ay, az);

    double vroll = msg.get()->angular_velocity.x;
    double vpitch = -1 * msg.get()->angular_velocity.y;
    double vyaw = -1 * msg.get()->angular_velocity.z;
    setUAVState_vel_angular(vroll, vpitch, vyaw);
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("UavState::imu_data_sub_callback");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

void UavState::imu_mag_sub_callback(const sensor_msgs::MagneticField::ConstPtr& msg)
{
  try
  {
    double mx = msg.get()->magnetic_field.y;
    double my = msg.get()->magnetic_field.x;
    double mz = -1 * msg.get()->magnetic_field.z;
    setMag(mx, my, mz);
    setMagCov(msg);
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("UavState::imu_mag_sub_callback");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

bool UavState::getGPSrefInit()
{
  boost::lock_guard<boost::mutex> lock(state_mutex_);
  return geo_ref_inited;
}

float UavState::getGPSrefAlt()
{
  boost::lock_guard<boost::mutex> lock(gps_mutex_);
  return ref_alt;
}

void UavState::StateMeasurementCallback(const nus_msgs::StateWithCovarianceStamped::ConstPtr& msg_ptr)
{
  try
  {
    // NWU to NED
    double x, y, z;
    linearCoordinateTF(msg_ptr->pose.pose.position.x, msg_ptr->pose.pose.position.y, msg_ptr->pose.pose.position.z, x,
                       y, z);
    setUAVState_xyz(x, -y, -z);

    double roll, pitch, yaw;
    angleTF(msg_ptr->pose.pose.orientation.x, msg_ptr->pose.pose.orientation.y, msg_ptr->pose.pose.orientation.z,
            msg_ptr->pose.pose.orientation.w, roll, pitch, yaw);
    setUAVState_abc(roll, -pitch, -yaw);
  }
  catch (const std::exception& exc)
  {
    ROS_ERROR("UavState::StateMeasurementCallback");
    std::cerr << exc.what() << std::endl;
    throw;
  }
}

}  // namespace nus_reference_generator
