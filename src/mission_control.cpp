// Copyright 2015-2017 Yuchao Hu
/*
 *  mission_control.cc
 *
 *  Created on: 2016
 *      Author: Yuchao Hu
 */

#include "nus_reference_generator/mission_control.h"

namespace nus_reference_generator
{
MissionControl::MissionControl() : node_(ros::NodeHandle("~"))
{
  // initialize timer
  nus_reference_generator::t_start = ros::Time::now().toSec();

  mavlink_ros_ptr_ = std::make_shared<MavlinkRos>(node_);
  uav_state_ptr_ = std::make_shared<UavState>(node_);
  reflexxes_generator_ptr_ = std::make_shared<ReflexxesGenerator>();
  navigator_ptr_ = std::make_shared<Navigator>(node_, mavlink_ros_ptr_, uav_state_ptr_, reflexxes_generator_ptr_);
  state_machine_ptr_ = std::make_shared<StateMachine>(node_, mavlink_ros_ptr_, uav_state_ptr_, navigator_ptr_);

  // Logger, initializes with pointer to all other modules
  // log_ptr_ = std::make_shared<Log>(node_, uav_state_ptr_, reflexxes_generator_ptr_, navigator_ptr_);

  // Start all modules
  // log_ptr_->Start();
  navigator_ptr_->Start();
  state_machine_ptr_->Start();
}

MissionControl::~MissionControl()
{
  ROS_INFO("mission_control finished");
}

}  // namespace nus_reference_generator
