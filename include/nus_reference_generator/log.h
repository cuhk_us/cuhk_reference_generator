// Copyright 2015-2017 Yuchao Hu
/*
 * log.h
 *
 *  Created on: Jul 16, 2015
 *      Author: li
 */

#ifndef NUS_REFERENCE_GENERATOR_LOG_H
#define NUS_REFERENCE_GENERATOR_LOG_H

#include <boost/thread.hpp>
#include <memory>
#include "nus_reference_generator/action.h"
#include "nus_reference_generator/basic_functions.h"
#include "nus_reference_generator/navigator.h"
#include "nus_reference_generator/reflexxes_generator.h"
#include "nus_reference_generator/uav_state.h"
#include <fstream>
#include <iostream>

namespace nus_reference_generator
{
//! State data logger
/*!
 * Logger for reference, measurement and behavior data.
 * Call Start() function after creating instance, a new thread will be created
 * to log data automatically.
 */

class Log
{
public:
  /*!
   * \brief Constructor
   * \param [in] _uav_state_ptr Pointer to UavState
   * \param [in] _reflexxes_generator_ptr Pointer to ReflexxesGenerator
   * \param [in] _navigator_ptr Pointer to Navigator
   */
  Log(ros::NodeHandle n,
      const std::shared_ptr<UavState>& _uav_state_ptr,
      const std::shared_ptr<ReflexxesGenerator>& _reflexxes_generator_ptr,
      const std::shared_ptr<Navigator>& _navigator_ptr);

  ~Log();

  /*!
   * \brief Start log thread
   * \param [in] _priority Priority of this log thread
   * \return Nothing
   */
  void Start();

  /*!
   * \brief Stop log thread
   * \return Nothing
   */
  void Stop();

private:
  /*!
   * \brief Main loop of this log thread
   * \return Nothing
   */
  void Loop(const ros::TimerEvent& event);

  /*!
   * \brief Log data into file
   * \param [in] file Object ofstream of file
   * \return Nothing
   */
  void LogData(std::ofstream& file);

private:
  std::shared_ptr<UavState> uav_state_ptr_;
  std::shared_ptr<ReflexxesGenerator> reflexxes_generator_ptr_;
  std::shared_ptr<Navigator> navigator_ptr_;

  std::ofstream logDataFile;
  bool keep_running_;
  ros::Timer main_loop_timer_;
};

}  // namespace nus_reference_generator

#endif  // NUS_REFERENCE_GENERATOR_LOG_H
