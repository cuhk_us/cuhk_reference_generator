// Copyright 2015-2017 Yuchao Hu
/*
 * uav_state.h
 *
 *  Created on: Aug 25, 2015
 *      Author: li
 */

#ifndef NUS_REFERENCE_GENERATOR_UAV_STATE_H
#define NUS_REFERENCE_GENERATOR_UAV_STATE_H

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Vector3Stamped.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/MagneticField.h>
#include <sensor_msgs/NavSatFix.h>
#include <std_msgs/Float64.h>
#include <tf/transform_datatypes.h>
#include <boost/thread/mutex.hpp>
#include <vector>
#include "geo/geo.h"
#include "nus_msgs/StateWithCovarianceStamped.h"
#include "nus_reference_generator/basic_functions.h"

#define PI 3.141592654

namespace nus_reference_generator
{
//! Uav state storage
/*!
 * A helper for reading and writing uav state.
 */
class UavState
{
private:
  UAV_STATE state;
  GPS_COORDINATE gps;
  MAGNETICFIELD mag;
  struct map_projection_reference_s ref;
  float ref_alt;
  bool geo_ref_inited;

  int pressed_status;
  int diff_count;

  ros::Subscriber local_sub;
  ros::Subscriber state_measurement_sub_;
  ros::Subscriber compass_hdg_sub;
  ros::Subscriber global_gps_vel_sub;
  ros::Subscriber global_gps_sub;
  ros::Subscriber imu_data_sub;
  ros::Subscriber imu_mag_sub;

  boost::mutex state_mutex_;
  boost::mutex gps_mutex_;
  boost::mutex mag_mutex_;

  double updateTime;

public:
  /*!
   * \brief Constructor
   * \param [in] n Ros node handler
   */
  explicit UavState(ros::NodeHandle n);

  ~UavState()
  {
  }

  /*!
   * \brief Get UAV state
   * \return UAV state
   */
  UAV_STATE getUAVState();

  /*!
   * \brief Set uav state, including position, velocity, acceleration and
   * attitude
   * \param [in] src UAV state
   * \return Nothing
   */
  void setUAVState(std::vector<double> src);

  /*!
   * \brief Set uav state position
   * \param [in] x UAV state position x, unit: meter
   * \param [in] y UAV state position y, unit: meter
   * \param [in] z UAV state position z, unit: meter
   * \return Nothing
   */
  void setUAVState_xyz(double x, double y, double z);

  /*!
   * \brief Set UAV state attitude
   * \param [in] roll UAV state attitude roll, unit: rad
   * \param [in] pitch UAV state attitude pitch, unit: rad
   * \param [in] yaw UAV state attitude yaw, unit: rad
   * \return Nothing
   */
  void setUAVState_abc(double roll, double pitch, double yaw);

  /*!
   * \brief Set UAV state heading
   * \param [in] c UAV state heading, unit: rad
   * \return Nothing
   */
  void setUAVState_heading(double c);

  /*!
   * \brief Set UAV state linear velocity
   * \param [in] vx UAV state linear velocity x, unit: meter
   * \param [in] vy UAV state linear velocity y, unit: meter
   * \param [in] vz UAV state linear velocity z, unit: meter
   * \return Nothing
   */
  void setUAVState_vel_linear(double vx, double vy, double vz);

  /*!
   * \brief Set UAV state angular velocity
   * \param [in] vroll UAV state angular velocity roll, unit: rad/s
   * \param [in] vpitch UAV state angular velocity pitch, unit: rad/s
   * \param [in] vyaw UAV state angular velocity yaw, unit: rad/s
   * \return Nothing
   */
  void setUAVState_vel_angular(double vroll, double vpitch, double vyaw);

  /*!
   * \brief Set UAV state linear acceleration
   * \param [in] ax UAV state linear acceleration x, unit: m/s^2
   * \param [in] ay UAV state linear acceleration y, unit: m/s^2
   * \param [in] az UAV state linear acceleration z, unit: m/s^2
   * \return Nothing
   */
  void setUAVState_acc_linear(double ax, double ay, double az);

  /*!
   * \brief Print uav state
   * \return Nothing
   */
  void print_state();

  /*!
   * \brief Get gps coordinate
   * \return GPS coordinate
   */
  GPS_COORDINATE getGPSCoordinate();

  /*!
   * \brief Set GPS coordinate
   * \param [in] latitude Latitude, unit: degree
   * \param [in] longitude Longitude, unit: degree
   * \param [in] altitude Altitude, unit: degree
   * \return Nothing
   */
  void setGPSCoordinate(double latitude, double longitude, double altitude);

  /*!
   * \brief Set GPS covariance status
   * \param [in] msg GPS message
   * \return Nothing
   */
  void setGPSCovStatus(const sensor_msgs::NavSatFix::ConstPtr& msg);

  /*!
   * \brief Get magneticfield data
   * \return Magneticfield data
   */
  MAGNETICFIELD getMag();

  /*!
   * \brief Set magneticfield data
   * \param [in] x Magneticfield x
   * \param [in] y Magneticfield y
   * \param [in] z Magneticfield z
   * \return Nothing
   */
  void setMag(double x, double y, double z);

  /*!
   * \brief Set magneticfield covariance
   * \param [in] msg Magneticfield message
   * \return Nothing
   */
  void setMagCov(const sensor_msgs::MagneticField::ConstPtr& msg);

  /*!
   * \brief Coordinate transform, ENU to NED, for mavlink ros communication
   * \param [in] x Position x in ENU
   * \param [in] y Position y in ENU
   * \param [in] z Position z in ENU
   * \param [out] x1 Position x1 in NED
   * \param [out] y1 Position y1 in NED
   * \param [out] z1 Position z1 in NED
   * \return Nothing
   */
  void linearCoordinateTF(double x, double y, double z, double& x1, double& y1, double& z1)
  {
    x1 = y;
    y1 = x;
    z1 = -z;
  }

  /*!
   * \brief Angle transform, quaternion to RPY
   * \param [in] x Quaternion x
   * \param [in] y Quaternion y
   * \param [in] z Quaternion z
   * \param [in] w Quaternion w
   * \param [out] r1 Euler roll, unit: rad
   * \param [out] p1 Euler pitch, unit: rad
   * \param [out] y1 Euler yaw, unit: rad
   * \return Nothing
   */
  void angleTF(double x, double y, double z, double w, double& r1, double& p1, double& y1);

  /*!
   * \brief Callback of local pose
   * \param [in] msg Local pose message
   * \return Nothing
   */
  void local_sub_callback(const geometry_msgs::PoseStamped::ConstPtr& msg);

  /*!
   * \brief Callback of compass heading
   * \param [in] msg Compass heading
   * \return Nothing
   */
  void compass_hdg_sub_callback(const std_msgs::Float64::ConstPtr& msg);

  /*!
   * \brief Callback of global gps velocity
   * \param [in] msg Global gps velocity message
   * \return Nothing
   */
  void global_gps_vel_sub_callback(const geometry_msgs::Vector3Stamped::ConstPtr& msg);

  /*!
   * \brief Callback of global gps status
   * \param [in] msg Global gps status
   * \return Nothing
   */
  void global_gps_sub_callback(const sensor_msgs::NavSatFix::ConstPtr& msg);

  /*!
   * \brief Callback of imu data
   * \param [in] msg Imu data message
   * \return Nothing
   */
  void imu_data_sub_callback(const sensor_msgs::Imu::ConstPtr& msg);

  /*!
   * \brief Callback of imu magnetic field data
   * \param [in] msg Imu magnetic field data
   * \return Nothing
   */
  void imu_mag_sub_callback(const sensor_msgs::MagneticField::ConstPtr& msg);

  // void setGPSref();

  /*!
   * \brief Get if gps reference initialized
   * \return True if gps ref initialized
   */
  bool getGPSrefInit();

  /*!
   * \brief Get gps reference altitude
   * \return Gps reference altitude, unit: meter
   */
  float getGPSrefAlt();

  /*!
   * \brief Get gps reference
   * \return Gps reference
   */
  map_projection_reference_s getGPSref();

  void StateMeasurementCallback(const nus_msgs::StateWithCovarianceStamped::ConstPtr& msg_ptr);
};

}  // namespace nus_reference_generator

#endif  // NUS_REFERENCE_GENERATOR_UAV_STATE_H
