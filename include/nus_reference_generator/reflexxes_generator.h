// Copyright 2015-2017 Yuchao Hu
/*
 * reflexxes_generator.h
 *
 *  Created on: Jul 14, 2015
 *      Author: li
 */

#ifndef NUS_REFERENCE_GENERATOR_REFLEXXES_GENERATOR_H
#define NUS_REFERENCE_GENERATOR_REFLEXXES_GENERATOR_H

#include <boost/thread/mutex.hpp>
#include <memory>
#include <vector>
#include "RMLPositionFlags.h"
#include "RMLPositionInputParameters.h"
#include "RMLPositionOutputParameters.h"
#include "ReflexxesAPI.h"
#include "mavlink/v1.0/common/mavlink.h"
#include "nus_reference_generator/basic_functions.h"
#include "nus_reference_generator/uav_state.h"

namespace nus_reference_generator
{
//! Generator for reflexxes reference
/*!
 * Use reflexxes to generate reflexxes reference based on uav state and target.
 */
class ReflexxesGenerator
{
public:
  enum GeneratorType
  {
    kTakeOff,
    kLand,
    kGps
  };

public:
  ReflexxesGenerator();
  ~ReflexxesGenerator();

  /*!
   * \brief Initialize reflexxes by uav state
   * \param [in] state Uav state
   * \return Nothing
   */
  void Initialize(const UAV_STATE& state);

  /*!
   * \brief Reset reflexxes by generator type
   * \param [in] _type Generator type
   * \return Nothing
   */
  void Reset(const GeneratorType& _type);

  /*!
   * \brief Reset reflexxes param by generator type
   * \param [in] _type Generator type
   * \return Nothing
   */
  void ResetLimit(const GeneratorType& _type);

  /*!
   * \brief Plan trajectory in simple type
   * \param [in] target Reference target
   * \return Reference in mavlinke format
   */
  mavlink_set_position_target_local_ned_t planningSimple(const REFERENCE& target);

  /*!
   * \brief Plan trajectory based on given uav state
   * \param [in] target Reference target
   * \param [in] state Uav state
   * \return Reference in mavlink format
   */
  mavlink_set_position_target_local_ned_t planningWithState(const REFERENCE& target, const UAV_STATE& state);

  /*!
   * \brief Get current reference in mavlink format
   * \param [out] current_ref Current reference in mavlink format
   * \return Nothing
   */
  void GetCurrentRef(mavlink_set_position_target_local_ned_t& current_ref) const;

  /*!
   * \brief Get reference position
   * \return Vector of reference position
   */
  std::vector<double> getRefPos();

  /*!
   * \brief Get reference velocity
   * \return Vector of reference velocity
   */
  std::vector<double> getRefVel();

  /*!
   * \brief Get reference acceleration
   * \return Vector of reference velocity
   */
  std::vector<double> getRefAcc();

  /*!
   * \brief Get reflexxes status
   * \return Reflesses status
   */
  int GetReflexxesStatus() const;

  /*!
   * \brief Set current position
   * \param [in] position Current position
   * \return Nothing
   */
  void SetCurrentPosition(const std::array<double, 2>& position);

private:
  /*!
   * \brief Set generated reference
   * \param [in] temp_ref Reference in mavlink format
   * \return Nothing
   */
  void SetGeneratedRef(mavlink_set_position_target_local_ned_t& temp_ref);

private:
  double pp_takeoff_maxVel_xy;
  double pp_takeoff_maxVel_z;
  double pp_takeoff_maxVel_c;
  double pp_takeoff_maxAcc_xy;
  double pp_takeoff_maxAcc_z;
  double pp_takeoff_maxAcc_c;
  double pp_takeoff_maxJerk_xy;
  double pp_takeoff_maxJerk_z;
  double pp_takeoff_maxJerk_c;

  double pp_land_maxVel_xy;
  double pp_land_maxVel_z;
  double pp_land_maxVel_c;
  double pp_land_maxAcc_xy;
  double pp_land_maxAcc_z;
  double pp_land_maxAcc_c;
  double pp_land_maxJerk_xy;
  double pp_land_maxJerk_z;
  double pp_land_maxJerk_c;

  double pp_fly_maxVel_xy;
  double pp_fly_maxVel_z;
  double pp_fly_maxVel_c;
  double pp_fly_maxAcc_xy;
  double pp_fly_maxAcc_z;
  double pp_fly_maxAcc_c;
  double pp_fly_maxJerk_xy;
  double pp_fly_maxJerk_z;
  double pp_fly_maxJerk_c;

  ReflexxesAPI* RML;               /*!< Reflexxes RML API variable */
  RMLPositionInputParameters* IP;  /*!< Reflexxes input variable */
  RMLPositionOutputParameters* OP; /*!< Reflexxes output variable */
  RMLPositionFlags Flags;          /*!< Reflexxes position flags */

  std::shared_ptr<ReflexxesAPI> reflexxes_heading_motion_ptr_;
  std::shared_ptr<RMLPositionInputParameters> reflexxes_heading_input_ptr_;
  std::shared_ptr<RMLPositionOutputParameters> reflexxes_heading_output_ptr_;
  RMLPositionFlags reflexxes_heading_flags_;

  ReflexxesAPI* velRML;               /*!< Reflexxes velocity API */
  RMLVelocityInputParameters* velIP;  /*!< Reflexxes velocity mode input */
  RMLVelocityOutputParameters* velOP; /*!< Reflexxes velocity mode output */
  RMLVelocityFlags velFlags;          /*!< Reflexxes velocity mode flags */

  double maxvel[4];
  double maxacc[4];
  double maxjerk[4];

  mavlink_set_position_target_local_ned_t temp_ref;

  boost::mutex pp_ref_mutex_;
  boost::mutex reflexxes_current_state_mutex_;

  bool is_initialized_;
  int reflexxes_status_;  // byc

  float uninpi_c;
};

}  // namespace nus_reference_generator

#endif  // NUS_REFERENCE_GENERATOR_REFLEXXES_GENERATOR_H
