// Copyright 2015-2017 Yuchao Hu
/*
 *  mavlink_ros.h
 *
 *  Created on: 2016
 *      Author: Yuchao Hu
 */

#ifndef NUS_REFERENCE_GENERATOR_MAVLINK_ROS_H
#define NUS_REFERENCE_GENERATOR_MAVLINK_ROS_H

#include <mavlink/v1.0/common/mavlink.h>
#include <ros/ros.h>
#include <string>
#include "mavros/Mavlink.h"

namespace nus_reference_generator
{
//! Helper for handling mavlink related functions
/*!
 * This class provides some useful mavlink related functions, just call them
 * after creating instance.
 */

class MavlinkRos
{
public:
  /*!
   * \brief Constructor
   * \param [in] n Ros node handler
   */
  explicit MavlinkRos(ros::NodeHandle n);

  /*!
   * \brief Display information on gcs
   * \param [in] info String of infomation
   * \param [in] severity Severity
   * \return Nothing
   */
  void disp_on_gcs(const std::string& info, const int& severity) const;

  /*!
   * \brief Send vehicle reference via mavlink
   * \param [in] ref_s Vehicle reference
   * \return Nothing
   */
  void send_ref(const mavlink_set_position_target_local_ned_t& ref_s) const;

private:
  /*!
   * \brief Convert mavlink message to ros format
   * \param [in] _mavmsg Mavlink message
   * \param [out] _rosmsg Ros message
   * \return Nothing
   */
  void copy_mavlink_to_ros(const mavlink_message_t& _mavmsg, mavros::Mavlink& _rosmsg) const;

private:
  ros::Publisher ros_to_pixhawk_ref_pub_;
  ros::Publisher ros_to_pixhawk_mea_pub_;
  ros::Publisher ros_to_gcs_pub_;

  uint8_t self_system_id_;
  uint8_t self_component_id_;
};

}  // namespace nus_reference_generator

#endif  // NUS_REFERENCE_GENERATOR_MAVLINK_ROS_H
