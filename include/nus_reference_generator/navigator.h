// Copyright 2015-2017 Yuchao Hu
/*
 *  navigator.h
 *
 *  Created on: 2016
 *      Author: Yuchao Hu
 */

#ifndef NUS_REFERENCE_GENERATOR_NAVIGATOR_H
#define NUS_REFERENCE_GENERATOR_NAVIGATOR_H

#include <geometry_msgs/Pose.h>
#include <math.h>
#include <mavlink/v1.0/common/mavlink.h>
#include <ros/ros.h>
#include <stdint.h>
#include <boost/thread.hpp>
#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include "nus_msgs/StateWithCovarianceStamped.h"
#include "nus_msgs/StateWithCovarianceMaskStamped.h"
#include "nus_reference_generator/action.h"
#include "nus_reference_generator/basic_functions.h"
#include "nus_reference_generator/mavlink_ros.h"
#include "nus_reference_generator/reflexxes_generator.h"
#include "nus_reference_generator/response.h"
#include "nus_reference_generator/uav_state.h"

namespace nus_reference_generator
{
//! Handler for behaviors
/*!
 * Handle different behaviors, and set target for reflexxes.
 * The reference from reflexxes and measurement are published.
 * Just call Start() after creating instance, the class will create a new thread
 * and handle the rest things.
 */

class Navigator
{
private:
  nus_reference_generator::action cur_action;
  nus_reference_generator::action last_action_msg_;
  nus_reference_generator::response cur_response;

  ros::Publisher response_pub;
  ros::Publisher state_reference_pub_;
  ros::Publisher state_reference_pose_pub_;
  ros::Subscriber action_sub;

  ros::Timer main_loop_timer_;

  bool init_flags[MAX_ACTION_NUMBER];
  bool report_finished_flag;
  std::vector<std::string> behavior_names;

  mavlink_set_position_target_local_ned_t ref_s;
  UAV_STATE uav_state;
  GPS_COORDINATE uav_gps_coordinate_;
  REFERENCE target;

  float home_x;
  float home_y;
  float home_z;
  double home_latitude_;
  double home_longitude_;
  double home_altitude_;
  float home_yaw;
  int count;

public:
  /*!
   * \brief Constructor
   * \param [in] n Ros node handler
   * \param [in] _mavlink_ros_ptr Pointer to mavlink_ros
   * \param [in] _uav_state_ptr Pointer to uav_state
   * \param [in] _reflexxes_generator_ptr Pointer to reflexxes_generator
   */
  Navigator(ros::NodeHandle n, const std::shared_ptr<MavlinkRos>& _mavlink_ros_ptr,
            const std::shared_ptr<UavState>& _uav_state_ptr,
            const std::shared_ptr<ReflexxesGenerator>& _reflexxes_generator_ptr);

  ~Navigator();

  /*!
   * \brief Start navigator thread
   * \param [in] _priority Priority of this thread
   * \return Nothing
   */
  void Start();

  /*!
   * \brief Stop navigator thread
   * \return Nothing
   */
  void Stop();

  /*!
   * \brief Get current action behavior index
   * \return Current action behavior index
   */
  int get_cur_behavior()
  {
    return cur_action.behavior;
  }

  /*!
   * \brief Get home position x
   * \return Home position x
   */
  float get_home_x() const
  {
    return home_x;
  }

  /*!
   * \brief Get home position y
   * \return Home position y
   */
  float get_home_y() const
  {
    return home_y;
  }

  /*!
   * \brief Get home position z
   * \return Home position z
   */
  float get_home_z() const
  {
    return home_z;
  }

  double GetHomeLatitude() const
  {
    return home_latitude_;
  }
  double GetHomeLongitude() const
  {
    return home_longitude_;
  }
  double GetHomeAltitude() const
  {
    return home_altitude_;
  }
  mavlink_set_position_target_local_ned_t GetUAVReference() const
  {
    return ref_s;
  }

private:
  /*!
   * \brief Main loop of navigator thread
   * \return Nothing
   */
  void Loop(const ros::TimerEvent& event);

  /*!
   * \brief Callback of vehicle action message from ros network
   * \param [in] msg Message of vehicle action
   * \return Nothing
   */
  void vehicle_action_callback(const nus_reference_generator::action::ConstPtr& msg);

  /*!
   * \brief Callback of servo imu message
   * \param [in] imuPtr Pointer to servo imu message
   * \return Nothing
   */
  void servoValueCallback(const sensor_msgs::Imu::ConstPtr& imuPtr);

  /*!
   * \brief Callback of state measurement message
   * \param [in] _msg_ptr Pointer to state measurement message
   * \return Nothing
   */
  void stateMeasurementCallback(const nus_msgs::StateWithCovarianceStamped::ConstPtr& _msg_ptr);

  /*!
   * \brief Clear all flags except specified one
   * \param [in] current_behavior Behavior index should not be cleared
   * \return Nothing
   */
  void clear_all_flags_except(int current_behavior);

  /*!
   * \brief Publish topic to report task has been finished
   * \param [in] behavior Index of behavior
   * \return Nothing
   */
  void report_task_finished(int behavior);

  void handle_takeoff();
  void handle_hfly();
  void handle_navigate_to();
  void handle_return_home();
  void handle_land();

private:
  ros::Subscriber servo_value_sub;
  ros::Subscriber state_measurement_sub_;

  double pitch_mag;
  double roll_mag;

  std::shared_ptr<MavlinkRos> mavlink_ros_ptr_;
  std::shared_ptr<UavState> uav_state_ptr_;
  std::shared_ptr<ReflexxesGenerator> reflexxes_generator_ptr_;
  nus_msgs::StateWithCovarianceStamped::ConstPtr last_state_measurement_ptr_;

  ros::Time land_reached_time_;
  bool is_land_reached_;

  bool keep_running_;
  bool NavigateWithTraj;
};

}  // namespace nus_reference_generator

#endif  // NUS_REFERENCE_GENERATOR_NAVIGATOR_H
