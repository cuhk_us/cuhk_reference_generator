// Copyright 2015-2017 Yuchao Hu
/*
 *  state_machine.h
 *
 *  Created on: Sep 11, 2016
 *      Author: Yuchao Hu
 */

#ifndef NUS_REFERENCE_GENERATOR_STATE_MACHINE_H
#define NUS_REFERENCE_GENERATOR_STATE_MACHINE_H

#include <geometry_msgs/PoseStamped.h>
#include <nus_msgs/StateStamped.h>
#include <boost/thread.hpp>
#include <memory>
#include "mavros/Cmd.h"
#include "nus_msgs/Gps2LocalPosition.h"
#include "nus_msgs/LocalPosition2Gps.h"
#include "nus_msgs/StartTaskManager.h"
#include "nus_reference_generator/action.h"
#include "nus_reference_generator/basic_functions.h"
#include "nus_reference_generator/mavlink_ros.h"
#include "nus_reference_generator/navigator.h"
#include "nus_reference_generator/response.h"
#include "nus_reference_generator/uav_state.h"

namespace nus_reference_generator
{
/*State machine elements*/
enum TaskState
{
  kUninit,
  kReady,
  kAutoReady,
  kTakeOff,
  kAutoTakeOff,
  kTakingOff,
  kAutoTakingOff,
  kHovering,
  kAutoHovering,
  kNavigate,
  kAutoNavigating,
  kNavigating,
  kAutoLand,
  kLand,
  kAutoLanding,
  kLanding,
  kAutoLanded,
  kLanded,
  kReturnHome
};

enum Plan
{
  kPlanAuto = 0,
  kPlanTakeOff = 1,
  kPlanNavigate = 2,
  kPlanLand = 3,
  kPlanForward = 4,
  kPlanBack = 5,
  kPlanLeft = 6,
  kPlanRight = 7,
  kPlanSetROI = 8,
  kPlanSetGPS = 9,
  kPlanGPSTakeOff = 11,
  kPlanGPSNavigate = 12,
  kPlanGPSLand = 13,
  kPlanSetTaskManagerPerchingPoint = 20
};

//! State machine for uav state switch
/*!
 * Wait for a certain command, and do the rest consequence state switch based on
 * uav state.
 * Just call Start() function after creating instance, a new thread will be
 * created to handle auto Update().
 */
class StateMachine
{
public:
  /*!
   * \brief Constructor
   * \param [in] n Ros node handler
   * \param [in] _mavlink_ros_ptr Pointer to MavlinkRos
   * \param [in] _uav_state_ptr Pointer to UavState
   */
  StateMachine(ros::NodeHandle n, const std::shared_ptr<MavlinkRos>& _mavlink_ros_ptr,
               const std::shared_ptr<UavState>& _uav_state_ptr, const std::shared_ptr<Navigator>& _navigator_ptr);

  ~StateMachine();

  /*!
   * \brief Start thread of state machine
   * \param [in] _priority Thread priority
   * \return Nothing
   */
  void Start();

  /*!
   * \brief Stop thread of state machine
   * \return Nothing
   */
  void Stop();

private:
  /*!
   * \brief Main loop of this thread: state machine
   * \return Nothing
   */
  void Loop(const ros::TimerEvent& event);

  void Update();

  /*!
   * \brief Callback of response
   * \param [in] msg Response message
   * \return Nothing
   */
  void ResponseCallback(const nus_reference_generator::response::ConstPtr& msg);

  /*!
   * \brief Callback of task manager
   * \param [in] msg Task manager output message
   * \return Nothing
   */
  void TaskManagerCallback(const nus_msgs::StateStamped::ConstPtr& msg);

  /*!
   * \brief Callback of mavros vehicle command message
   * \param [in] _msg_ptr Pointer to mavros command message
   * \return Nothing
   */
  void VehicleCmdCallback(const mavros::Cmd::ConstPtr& _msg_ptr);

  /*!
   * \brief Handler of plan cmd
   * \param [in] cmd Command message pointer
   * \return Nothing
   */
  void HandlePlanCmd(const mavros::Cmd::ConstPtr& _msg_ptr);

  void Geofencing();

  bool Gps2LocalPosition(nus_msgs::Gps2LocalPosition::Request& req, nus_msgs::Gps2LocalPosition::Response& res);
  bool LocalPosition2Gps(nus_msgs::LocalPosition2Gps::Request& req, nus_msgs::LocalPosition2Gps::Response& res);

private:
  /*Private variables*/
  // system initialization subtask variables
  UAV_STATE uav_state_;
  TaskState task_state_;

  ros::Subscriber response_sub_;
  ros::Subscriber vehicle_cmd_sub_;
  ros::Subscriber task_manager_sub_;
  ros::Publisher task_manager_perching_point_pub_;
  ros::Publisher action_pub_;
  ros::Publisher roi_pub_;
  ros::ServiceClient mavros_set_home_client_;
  ros::ServiceServer gps_to_local_position_server_;
  ros::ServiceServer local_position_to_gps_server_;
  ros::ServiceClient task_manager_start_client_;
  ros::Timer main_loop_timer_;
  
  std::shared_ptr<MavlinkRos> mavlink_ros_ptr_;
  std::shared_ptr<UavState> uav_state_ptr_;
  std::shared_ptr<Navigator> navigator_ptr_;

  uint8_t self_system_id_;
  bool enable_geofencing_;

  // thread
  bool keep_running_;
  std::shared_ptr<boost::thread> thread_point_;

  boost::mutex task_state_mutex_;
};  // class StateMachine

}  // namespace nus_reference_generator

#endif  // NUS_REFERENCE_GENERATOR_STATE_MACHINE_H
