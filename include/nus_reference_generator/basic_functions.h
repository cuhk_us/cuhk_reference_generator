// Copyright 2015-2017 Yuchao Hu
/*
 * basic_functions.h
 *
 *  Created on: Jul 12, 2015
 *      Author: li
 */

#ifndef NUS_REFERENCE_GENERATOR_BASIC_FUNCTIONS_H
#define NUS_REFERENCE_GENERATOR_BASIC_FUNCTIONS_H

#include <ros/ros.h>

#define FREQUENCY 50
#define CYCLE_TIME 1.0 / FREQUENCY

#define IDLE 0
#define TAKEOFF 1
#define HFLY 2
#define NAVIGATE_TO 3
#define VISION_GUIDE_DESCEND 4
#define DESCEND_GRASP_ASCEND 5
#define DESCEND_UNLOAD_ASCEND 6
#define RETURN_HOME 7
#define LAND 8
#define OPEN_CLAW 9
#define CLOSE_CLAW 10
#define MAX_ACTION_NUMBER 20

namespace nus_reference_generator
{
// ------------------------ parameters

// for visual guidance
extern double return_home_height;

// for mission
extern double takeoff_height;

/*Mission constants*/
/*Configurable constant and parameters are listed here*/

// home position(on land) in global frame
extern double homeposition[3];

// ------------------------ parameters end

// --------------------------------------- ros topics -----------
#define INPI(angle) (angle -= floor((angle + M_PI) / (2 * M_PI)) * 2 * M_PI)

float DEG2RAD(float angle);

float RAD2DEG(float angle);

struct MEASUREMENT
{
  float x, y, z, c;
};

struct REFERENCE
{
  float x, y, z, c, vx, vy, vz, r;
  float period;
};

struct UAV_STATE
{
  // in NED if outdoor
  float x_pos;
  float y_pos;
  float z_pos;
  float c_pos;

  float x_vel;
  float y_vel;
  float z_vel;
  float c_vel;

  float x_acc;
  float y_acc;
  float z_acc;
  float c_acc;

  float roll;
  float pitch;
  float yaw;

  float roll_vel;
  float pitch_vel;
  float yaw_vel;
};

struct GPS_COORDINATE
{
  double latitude;
  double longitude;
  double altitude;

  double position_cov[9];

  int service;
  int status;
};

struct MAGNETICFIELD
{
  double x;
  double y;
  double z;
  double mag_cov[9];
};

struct WayPnt
{
  double x;
  double y;
  double z;
  double c;
  double period;
  int target;
  int type;
};

extern double t_start;

double getElapsedTime();

}  // namespace nus_reference_generator

#endif  // NUS_REFERENCE_GENERATOR_BASIC_FUNCTIONS_H
