// Copyright 2015-2017 Yuchao Hu
/*
 *  mission_control.h
 *
 *  Created on: 2016
 *      Author: Yuchao Hu
 */

#ifndef NUS_REFERENCE_GENERATOR_MISSION_CONTROL_H
#define NUS_REFERENCE_GENERATOR_MISSION_CONTROL_H

#include <ros/ros.h>
#include <memory>
#include "nus_reference_generator/log.h"
#include "nus_reference_generator/mavlink_ros.h"
#include "nus_reference_generator/navigator.h"
#include "nus_reference_generator/reflexxes_generator.h"
#include "nus_reference_generator/state_machine.h"
#include "nus_reference_generator/uav_state.h"

namespace nus_reference_generator
{
//! Main class for this node.
/*!
 * This class is for global initialization, which creates and initializes all
 * the instances of MavlinkRos, UavState, ReflexxesGenerator, Navigator,
 * StateMachine and Log.
 */

class MissionControl
{
public:
  MissionControl();
  ~MissionControl();

private:
  ros::NodeHandle node_;

  std::shared_ptr<MavlinkRos> mavlink_ros_ptr_;
  std::shared_ptr<UavState> uav_state_ptr_;
  std::shared_ptr<ReflexxesGenerator> reflexxes_generator_ptr_;
  std::shared_ptr<Navigator> navigator_ptr_;
  std::shared_ptr<StateMachine> state_machine_ptr_;
  std::shared_ptr<Log> log_ptr_;
};

}  // namespace nus_reference_generator

#endif  // NUS_REFERENCE_GENERATOR_MISSION_CONTROL_H
